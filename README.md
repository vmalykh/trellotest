  - File in any directory with provided environment variable `CREDS` with path to it
  - Envorironment variables equals to properties names

### File "credentials.properties" in classpath

File name must be equals to `credentials.properties` and have 5 properties:
| Property name | Value |
|---------------| ------|
| credentials.username | user name to sign in |
| credentials.email | user email |
| credentials.password | with password |
| api.key | account api key, can be obtained here: [https://trello.com/app-key](https://trello.com/app-key) |
| api.token | account api secret token, can be obtained here : [https://trello.com/app-key](https://trello.com/app-key) by clicking link "authorize" |


### File in any directory:
  You can give any name of file or location and provide environment variable `CREDS` with path to it. Contains of file must be equals previous section.
  **HINT**: You can automaticaly set this variable with jira plugin "Credentials Binding Plugin"


### Environment variables:
  You can provide credentials bu enrironment variables. Variables names and his values must be equals properties from table in first section
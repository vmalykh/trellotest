package com.pflb.ttipoi.pages;

import com.pflb.ttipoi.exceptions.InvalidOperationException;
import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.interfaces.Clickable;
import com.pflb.ttipoi.interfaces.Fillable;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

public abstract class AbstractPage implements Fillable, Clickable {
    protected WebDriver driver = Driver.get();
    protected WebDriverWait wait = new WebDriverWait(driver, 10);
    protected Logger logger = LoggerFactory.getLogger(getClass());
    protected Marker marker = MarkerFactory.getMarker("PAGE/ABSTRACT");

    public AbstractPage() {
        logger.debug(marker, "Initializing page object class {}", getClass());
        PageFactory.initElements(driver, this);
    }

    @Override
    public void fill(String fieldName, String value) {
        logger.debug(marker, "Filling \"{}\" with value \"{}\"", fieldName, value);
        Class<?> klass = this.getClass();
        Field[] fields = klass.getDeclaredFields();
        Optional<Field> optionalField = Arrays.stream(fields)
                .filter(f -> f.isAnnotationPresent(TextField.class) && f.getAnnotation(TextField.class).value().equals(fieldName))
                .findFirst();
        if (!optionalField.isPresent()) throw new InvalidOperationException(String.format("Field annotated @TextField with name \"%s\" can't be found in class \"%s\"",fieldName, this.getClass().getSimpleName()));
        Field field = optionalField.get();
        field.setAccessible(true);
        try {
            Object fieldValue = field.get(this);
            Method skMethod = fieldValue.getClass().getMethod("sendKeys", CharSequence[].class);
            Method clearMethod = fieldValue.getClass().getMethod("clear");
            CharSequence[] vti = new String[]{value};
            clearMethod.invoke(fieldValue);
            skMethod.invoke(fieldValue, new Object[]{vti});
        }
        catch (IllegalAccessException e) { throw new RuntimeException("Cannot get access to field", e); }
        catch (NoSuchMethodException e) { throw new RuntimeException("Cannot find method", e); }
        catch (InvocationTargetException e) { throw new RuntimeException("Cannot execute method", e); }
    }


    @Override
    public void click(String buttonName) {
        logger.debug(marker, "Clicking on \"{}\" ", buttonName);
        Class<?> klass = this.getClass();
        Field[] fields = klass.getDeclaredFields();
        Optional<Field> optionalField = Arrays.stream(fields)
                .filter(f -> f.isAnnotationPresent(Button.class) && f.getAnnotation(Button.class).value().equals(buttonName))
                .findFirst();
        if (!optionalField.isPresent()) throw new InvalidOperationException(String.format("Field annotated @Button with name \"%s\" can't be found in class \"%s\"",buttonName, this.getClass().getSimpleName()));
        Field field = optionalField.get();
        field.setAccessible(true);
        try {
            Object fieldValue = field.get(this);
            Method method = fieldValue.getClass().getMethod("click");
            method.invoke(fieldValue);
        }
        catch (IllegalAccessException e) { throw new RuntimeException("Cannot get access to field", e); }
        catch (NoSuchMethodException e) { throw new RuntimeException("Cannot find method", e); }
        catch (InvocationTargetException e) { throw new RuntimeException("Cannot execute method", e); }
    }
}



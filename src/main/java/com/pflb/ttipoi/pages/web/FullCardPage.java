package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.interfaces.CheckList;
import com.pflb.ttipoi.interfaces.HasCheckList;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FullCardPage extends AbstractWebPage implements HasCheckList {
    @Button("Rename")
    @TextField("Card title")
    @FindBy(css = ".window-title textarea")
    private WebElement title;

    @Button("Close")
    @FindBy(css = ".dialog-close-button")
    private WebElement btnClose;

    @Button("Change description")
    @FindBy(css = ".description-content")
    private WebElement btnDescriptionChange;

    @TextField("Description")
    @FindBy(css = ".description-edit textarea")
    private WebElement txtDescription;

    @FindBy(css = ".checklist")
    private WebElement checkList;

    @Button("Checklist")
    @FindBy(css = ".js-add-checklist-menu")
    private WebElement btnAddChecklist;

    @Button("Save description")
    @FindBy(css = ".description-edit input[type=submit]")
    private WebElement btnDescriptionSubmit;

    @Button("Archive")
    @FindBy(css = "a.js-archive-card")
    private WebElement btnArchive;

    @Button("Delete")
    @FindBy(css = ".js-delete-card")
    private WebElement btnDelete;


    public FullCardPage() {
        logger.debug(marker, "Initializing FullCardPage...");
        WebElement searchContext = Driver.get().findElement(By.cssSelector(".window"));
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
        PageFactory.initElements(searchFactory, this);
    }

    @Override
    public boolean hasCheckList() {
        return checkList.isDisplayed();
    }

    @Override
    public CheckList getCheckList() {
        return new CardCheckListPage(checkList);
    }
}

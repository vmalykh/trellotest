package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.pages.AbstractPage;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public class AbstractWebPage extends AbstractPage {
    protected Marker marker = MarkerFactory.getMarker("PAGES/WEB");
}

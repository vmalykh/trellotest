package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.interfaces.HasMenu;
import com.pflb.ttipoi.pages.annotations.Button;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends AbstractWebPage implements HasMenu {
    @Button("Boards")
    @FindBy(css = "[data-test-id=\"header-boards-menu-button\"]")
    private WebElement btnBoards;

    @Button("User menu")
    @FindBy(css = "[data-test-id=\"header-member-menu-button\"]")
    private WebElement btnUserMenu;

    @Button("Add board")
    @FindBy(css = ".board-tile.mod-add")
    private WebElement btnAddBoard;

    @FindBy(css = ".home-container")
    private WebElement homeRoot;

    @Override
    public void invokeMenu() {
        logger.debug(marker, "Invoking user menu");
        btnUserMenu.click();
    }

    public MainPage() {
        super();
        wait.until((d) -> homeRoot.isDisplayed());
    }
}

package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DialogPage extends AbstractWebPage {
    @TextField("Title")
    @FindBy(id = "id-checklist")
    private WebElement txtChecklistTitle;

    @TextField("Board title")
    @FindBy(css = ".js-board-name-input")
    private WebElement txtBoardTitle;

    @Button("Confirm")
    @FindBy(css = "input[type=submit]")
    private WebElement btnSubmit;

    public DialogPage() {
        logger.debug(marker, "Initializing Dialog...");
        WebElement searchContext = Driver.get().findElement(By.cssSelector(".pop-over"));
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
        PageFactory.initElements(searchFactory, this);
    }
}

package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.pages.AbstractPage;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractPage {
    @TextField("Username")
    @FindBy(id = "user")
    WebElement txtUser;

    @TextField("Password")
    @FindBy(id = "password")
    WebElement txtPassword;

    @Button("Login")
    @FindBy(id = "login")
    WebElement btnSubmit;
}

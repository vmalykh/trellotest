package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.helpers.SearchPolicy;
import com.pflb.ttipoi.interfaces.HasCards;
import com.pflb.ttipoi.interfaces.HasLists;
import com.pflb.ttipoi.interfaces.HasTitle;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BoardPage extends AbstractWebPage implements HasTitle, HasLists {
    @FindBy(css = ".board-wrapper")
    private WebElement pageRoot;

    @Button("Back")
    @FindBy(css = "[name=house]")
    private WebElement btnBack;

    @Button("Close")
    @FindBy(css = ".js-add-list .js-cancel-edit")
    private WebElement btnClose;

    @Button("Rename")
    @FindBy(css = ".js-rename-board")
    private WebElement btnRename;

    @TextField("Board title")
    @FindBy(css = ".js-board-name-input")
    private WebElement txtBoardTitle;

    @Button("Add a list")
    @FindBy(css = ".js-add-list")
    private WebElement btnAddList;

    @TextField("List name")
    @FindBy(css = ".list-name-input")
    private WebElement txtListname;

    @Button("Save")
    @FindBy(css = ".js-add-list input[type=submit]")
    private WebElement btnSave;

    @FindBy(css = ".board-header span")
    private WebElement title;

    @FindBy(css = ".list")
    private List<WebElement> lists;

    @Override
    public String getTitle() {
        return title.getText();
    }

    public List<HasCards> getLists() {
        logger.debug(marker, "Searching for all lists");
        return lists.stream().map(ListPage::new).collect(Collectors.toList());
    }

    public HasCards getListByName(String listName, SearchPolicy searchPolicy) {
        Optional<HasCards> list = getLists().stream().filter(lst -> searchPolicy.predicate().test(lst.getTitle(), listName)).findFirst();
        if (!list.isPresent()) throw new NotFoundException(String.format("List with name \"%s\" not fount on board \"%s\"", listName, getTitle()));
        return list.get();
    }

    public BoardPage() {
        super();
        wait.until((d) -> pageRoot.isDisplayed());
    }
}

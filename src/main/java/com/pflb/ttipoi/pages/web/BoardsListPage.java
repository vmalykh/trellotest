package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.interfaces.HasBoards;
import com.pflb.ttipoi.interfaces.HasMenu;
import com.pflb.ttipoi.pages.annotations.Button;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BoardsListPage extends AbstractWebPage implements HasBoards{
    @FindBy(css = "[data-react-beautiful-dnd-droppable='1'] > div")
    private List<WebElement> boards;

    public BoardsListPage() {
        logger.debug(marker, "Initializing BoardsList page...");
        WebElement context = Driver.get().findElement(By.cssSelector("[data-test-id=\"header-boards-menu-popover\"]"));
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(context);
        PageFactory.initElements(searchFactory, this);
    }

    @Override
    public List<String> getAllBoardNames() {
        return boards.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    @Override
    public void selectBoard(String name) {
        logger.debug(marker, "Searching for board \"{}\"", name);
        Optional<WebElement> optionalBoard = boards.stream().filter(b -> b.getText().equals(name)).findFirst();
        if (!optionalBoard.isPresent()) throw new NotFoundException(String.format("Board with name \"%s\" not found", name));
        optionalBoard.get().click();
    }
}

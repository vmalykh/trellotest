package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateBoardDialog extends AbstractWebPage {
    @TextField("Board name")
    @FindBy(css = "input")
    WebElement txtBoardName;

    @Button("Create board")
    @FindBy(css = "button[type=submit]")
    WebElement btnSubmit;

    public CreateBoardDialog() {
        logger.debug(marker, "Initializing CreateBoardDialog page...");
        WebElement context = Driver.get().findElement(By.cssSelector(".create-board-form"));
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(context);
        PageFactory.initElements(searchFactory, this);
    }
}

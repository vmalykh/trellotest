package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.helpers.SearchPolicy;
import com.pflb.ttipoi.interfaces.HasCards;
import com.pflb.ttipoi.interfaces.HasTitle;
import com.pflb.ttipoi.interfaces.SmallCard;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ListPage extends AbstractWebPage implements HasTitle, HasCards {
    @TextField("List name")
    @FindBy(css = ".list-header-name")
    private WebElement title;

    @Button("Rename")
    @FindBy(css = ".list-header-target")
    private WebElement btnRename;

    @Button("Add a card")
    @FindBy(css = ".open-card-composer")
    private WebElement btnAddCard;

    @Button("Add")
    @FindBy(css = "input[type=submit]")
    private WebElement btnSubmit;

    @TextField("Card name")
    @FindBy(css = ".list-card-composer-textarea")
    private WebElement txtNewCardName;

    @FindBy(css = ".list-card")
    private List<WebElement> cards;

    private ListPage() { }

    ListPage(WebElement searchContext) {
        logger.debug(marker, "Initializing List...");
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
        PageFactory.initElements(searchFactory, this);
    }

    @Override
    public String getTitle() {
        return title.getAttribute("value");
    }

    @Override
    public List<SmallCard> getCards() {
        return cards.stream().map(SmallCardPage::new).collect(Collectors.toList());
    }

    @Override
    public SmallCard getCardByName(String cardName, SearchPolicy searchPolicy) {
        logger.debug(marker, "Searching for card {} \"{}\" on list \"{}\"", searchPolicy, cardName, getTitle());
        Optional<SmallCard> optionalCard = getCards().stream().filter(card -> searchPolicy.predicate().test(cardName, card.getTitle())).findFirst();
        if (!optionalCard.isPresent()) throw new NotFoundException(String.format("Card with name \"%s\" not fount in list \"%s\"", cardName, getTitle()));
        return optionalCard.get();
    }

    WebElement getDropElement(DropLocation location) {
        logger.debug("Requested drop location {} on list \"{}\"", location, getTitle());
        if (location.equals(DropLocation.TOP)) return title;
        else return btnAddCard;
    }

    public enum DropLocation {
        TOP, BOTTOM
    }
}

package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.interfaces.Selectable;
import com.pflb.ttipoi.interfaces.SmallCard;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SmallCardPage extends AbstractWebPage implements SmallCard, Selectable {

    @FindBy(css = ".list-card-title")
    private WebElement title;

    @FindBy(css = ".icon-description")
    private WebElement decriptionIcon;

    @FindBy(css = ".icon-checklist")
    private WebElement checklistIcon;

    @FindBy(css = ".badge-text")
    private WebElement checkedCount;

    private SmallCardPage() {}

    SmallCardPage(WebElement searchContext) {
        logger.debug(marker, "Initializing Small card...");
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
        PageFactory.initElements(searchFactory, this);
    }

    @Override
    public boolean hasDescriptionIcon() {
        logger.debug(marker, "Trying to obtain description icon");
        try {
            return decriptionIcon.isDisplayed();
        } catch (NoSuchElementException e) {
            logger.trace(marker, "Description icon not found" ,e);
        }
        return false;
    }

    @Override
    public boolean hasChecklistIcon() {
        logger.debug(marker, "Trying to obtain checklist icon");
        try {
            return checklistIcon.isDisplayed();
        } catch (NoSuchElementException e) {
            logger.trace(marker, "Checklist icon not found", e);
        }
        return false;
    }

    @Override
    public String getTitle() {
        return title.getText();
    }

    @Override
    public int[] getCheckedCount() {
        logger.debug(marker, "Trying to obtain checked count");
        try {
            int[] result = new int[2];
            String text = checkedCount.getText();
            String[] strings = text.split("/");
            for (int i = 0; i < strings.length; i++) {
                result[i] = Integer.valueOf(strings[i]);
            }
            logger.trace(marker, "Checked count: {}", result);
            return result;
        } catch (NoSuchElementException e) {
            logger.trace(marker, "Checklist container not found", e);
            throw new IllegalStateException("Checklist container not found", e);
        }
    }

    @Override
    public void select() {
        title.click();
    }

    public void moveTo(ListPage list, ListPage.DropLocation location) {
        logger.debug(marker, "Moving card \"{}\" to list \"{}\"", getTitle(), list.getTitle());
        WebElement dropElement = list.getDropElement(location);
        new Actions(driver).clickAndHold(title).moveToElement(dropElement).release().perform();
    }
}

package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.interfaces.CheckList;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

public class CardCheckListPage extends AbstractWebPage implements CheckList {
    @FindBy(css = ".checklist-item")
    private List<WebElement> items;

    @FindBy(css = ".checklist-title h3")
    private WebElement title;

    @FindBy(css = ".checklist-progress-percentage")
    private WebElement progressPercent;

    @Button("Add an item")
    @FindBy(css = ".js-new-checklist-item-button")
    private WebElement btnAddItem;

    @TextField("Item name")
    @FindBy(css = ".checklist-new-item-text")
    private WebElement txtItemName;

    @FindBy(css = ".checklist-progress-bar")
    private WebElement progressBar;

    @Button("Add")
    @FindBy(css = ".checklist-new-item .checklist-add-controls input[type=submit]")
    private WebElement btnSubmit;

    private CardCheckListPage() {}

    CardCheckListPage(WebElement searchContext) {
        logger.debug(marker, "Initializing CardCheckList...");
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
        PageFactory.initElements(searchFactory, this);
    }

    @Override
    public List<CheckList.Item> getItems() {
        return items.stream().map(Item::new).collect(Collectors.toList());
    }

    @Override
    public String getTitle() {
        return title.getText();
    }

    @Override
    public void setItemState(String itemName, boolean state) {
//        Optional<CheckList.Item> clItem = getItems().stream().filter(item -> item.getTitle().equals(itemName)).findFirst();
        WebElement itemElement = null;
        Item itemObject = null;
        for (WebElement item : items) {
            itemObject = new Item(item);
            if (!itemObject.getTitle().equals(itemName)) continue;
            itemElement = item;
            break;
        }
        if (itemElement == null) throw new NotFoundException(String.format("Item with name \"%s\" not found in checklist \"%s\"", itemName, getTitle()));
        if (itemElement.getAttribute("class").contains("checklist-item-state-complete") != state) {
            itemObject.changeState();
        }
        WebElement finalItemElement = itemElement;
        wait.until(d->
                finalItemElement.getAttribute("class").contains("checklist-item-state-complete") == state
        );

    }

    public static class Item extends AbstractWebPage implements CheckList.Item {
        @FindBy(css = ".checklist-item-checkbox")
        private WebElement checkbox;

        @FindBy(css = ".checklist-item-details-text")
        private WebElement name;

        private WebElement context;

        private Item() {}

        private Item(WebElement searchContext) {
            logger.debug(marker, "Initializing Cheklist item...");
            this.context = searchContext;
            SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
            PageFactory.initElements(searchFactory, this);
        }

//        @Override
//        public void setChecked(boolean state) {
//            logger.debug(marker, "Setting state of checkbox item \"{}\" to {}", getTitle(), state);
//            if (context.getAttribute("class").contains("checklist-item-state-complete") != state) checkbox.click();
//            wait.until(d -> context.getAttribute("class").contains("checklist-item-state-complete") == state);
//        }

        private void changeState() {
            checkbox.click();
        }

        @Override
        public String getTitle() {
            return name.getText();
        }
    }
}

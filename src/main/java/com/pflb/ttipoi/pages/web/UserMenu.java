package com.pflb.ttipoi.pages.web;

import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.interfaces.Menu;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Optional;

public class UserMenu extends AbstractWebPage implements Menu {
    @FindBy(css = "nav a, nav button")
    private List<WebElement> items;

    public UserMenu() {
        logger.debug(marker, "Initializing USerMenu...");
        WebElement searchContext = driver.findElement(By.cssSelector("[data-test-id=\"header-member-menu-popover\"]"));
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
        PageFactory.initElements(searchFactory, this);
    }

    @Override
    public void selectMenuEntry(String text) {
        logger.debug(marker, "Searching for menu element \"{}\"", text);
        Optional<WebElement> foundElement = items.stream().filter(item -> item.getText().equals(text)).findFirst();
        if (!foundElement.isPresent()) throw new NotFoundException(String.format("Menu item \"%s\" not found", text));
        foundElement.get().click();
    }
}

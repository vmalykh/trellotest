package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.helpers.SearchPolicy;
import com.pflb.ttipoi.interfaces.HasCards;
import com.pflb.ttipoi.interfaces.HasTitle;
import com.pflb.ttipoi.interfaces.SmallCard;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ListPage extends AbstractAndroidPage implements HasTitle, HasCards {
    private WebElement context;

    @Button("List title")
    @TextField("List title")
    @FindBy(id = "list_name")
    private WebElement lblListTitle;

    @Button("Add card")
    @FindBy(id = "add_button")
    private WebElement btnAddCard;

    @FindBy(id = "card_proper")
    private List<WebElement> cards;

    @Override
    public SmallCard getCardByName(String cardName, SearchPolicy searchPolicy) {
        logger.debug(marker, "Searching card {}, \"{}\"", searchPolicy, cardName);
        List<SmallCard> cards = getCards();
        Optional<SmallCard> card = cards.stream().filter(c -> searchPolicy.predicate().test(c.getTitle(), cardName)).findFirst();
        if (!card.isPresent()) throw new NotFoundException(String.format("Card with name \"%s\" not found on list \"%s\"", cardName, getTitle()));
        return card.get();
    }

    public List<SmallCard> getCards() {
        logger.debug(marker, "Getting all cards on list \"{}\"", getTitle());
        ArrayList<SmallCard> result = new ArrayList<>();
        cards.forEach(c -> result.add(new SmallCardPage(c)));
        return result;
    }

    ListPage(WebElement searchContext) {
        logger.debug(marker, "Initializing LList page");
        this.context = searchContext;
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
        PageFactory.initElements(searchFactory, this);
    }

    @Override
    public String getTitle() {
        return lblListTitle.getText();
    }
}

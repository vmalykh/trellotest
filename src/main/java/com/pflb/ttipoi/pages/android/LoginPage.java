package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.interfaces.Clickable;
import com.pflb.ttipoi.interfaces.Fillable;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractAndroidPage implements Clickable, Fillable {
    @TextField("Username")
    @FindBy(id = "com.trello:id/user")
    private WebElement txtUser;

    @TextField("Password")
    @FindBy(id = "com.trello:id/password")
    private WebElement pwdPassword;

    @Button("Cancel")
    @FindBy(id = "com.trello:id/cancel")
    private WebElement btnCancel;

    @Button("Submit")
    @FindBy(id = "com.trello:id/authenticate")
    private WebElement btnSubmit;
}

package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditDialogPage extends AbstractAndroidPage {
    @TextField("Name")
    @FindBy(id = "edit")
    WebElement txtName;

    @Button("Ok")
    @FindBy(id = "button1")
    WebElement btnOk;

    @Button("Cancel")
    @FindBy(id = "button2")
    WebElement btnCancel;
}

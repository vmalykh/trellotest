package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.helpers.Config;
import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.helpers.SearchPolicy;
import com.pflb.ttipoi.helpers.repositories.PageStore;
import com.pflb.ttipoi.interfaces.HasLists;
import com.pflb.ttipoi.interfaces.HasTitle;
import com.pflb.ttipoi.interfaces.Scrollabe;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MarkerFactory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BoardPage extends AbstractAndroidPage implements HasTitle, Scrollabe, HasLists {
    @Button("Back")
    @FindBy(xpath= "//*[@resource-id='com.trello:id/action_bar']/android.widget.ImageButton")
    private WebElement btnBack;

    @Button("Menu")
    @FindBy(id = "board_sections")
    private WebElement btnMenu;

    @FindBy(id = "toolbar_title")
    private WebElement titleContainer;

    @FindBy(xpath = "//*[@resource-id='com.trello:id/card_list_container' and descendant::android.widget.FrameLayout[@resource-id='com.trello:id/list_actions_button'] and descendant::android.widget.EditText]")
    private List<WebElement> cardsLists;

    @Button("Add list")
    @FindBy(id = "add_list_button")
    private WebElement btnAddList;

    @TextField("List name")
    @FindBy(id = "list_name_edit_text")
    private WebElement txtListName;

    @Button("Edit cancel")
    @FindBy(xpath = "//*[@resource-id='com.trello:id/editing_toolbar']/android.widget.ImageButton")
    private WebElement btnCancel;

    @Button("Edit confirm")
    @FindBy(id = "confirm")
    private WebElement btnConfirm;

    private List<ListPage> getVisibleLists() {
        logger.trace(marker, "Obtaining visible lists");
        By locator;
        try {
            Field cardsListsField = this.getClass().getDeclaredField("cardsLists");
            FindBy findBy = cardsListsField.getAnnotation(FindBy.class);
            locator = By.xpath(findBy.xpath());
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException("Somthing went wrong, field not found!", e);
        }
        List lists = driver.findElements(locator);
        List<ListPage> result = new ArrayList<>();
        for (Object list : lists) result.add(new ListPage((WebElement)list));
        return result;
    }

    /**
     * Scrolls given number of pages or to end
     * @param direction scroll direction
     * @param count count of pages, set -1 for scroll to end
     */
    @Override
    public void scroll(ScrollDirection direction, int count) {
        logger.debug(marker, "Scrolling to {}, direction: {}", count == -1 ? "end of screen" : count + " pages", direction);
        Dimension wndSize = Driver.get().manage().window().getSize();
        int startx = 0, starty = 0, endx = 0, endy = 0;
        switch (direction) {
            case RIGHT:
                startx = (int)Math.round(wndSize.width * 0.9);
                endx = -startx;
                starty = (int)Math.round(wndSize.height * 0.5);
                break;

            case LEFT:
                startx = (int)Math.round(wndSize.width * 0.1);
                endx = (int)Math.round(wndSize.width * 0.8);
                starty = (int)Math.round(wndSize.height * 0.5);
                break;

            case UP:
                starty = (int)Math.round(wndSize.height * 0.1);
                endy = (int)Math.round(wndSize.height * 0.9);
                startx = (int)Math.round(wndSize.width * 0.5);
                break;

            case DOWN:
                starty = (int)Math.round(wndSize.height * 0.9);
                endy = -starty;
                startx = (int)Math.round(wndSize.width * 0.5);
        }
        By locatorToStopSearch = getStopLocator(direction);
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS); //disable timeouts for search
        int timeout = Config.getGlobalTimeout();
        long endTime = System.currentTimeMillis() + timeout * 1000;
        boolean foundFlag = false;
        int counter = 0;

        while (true) {
            try {//try to find button
                logger.trace(marker, "Trying to find last element on page");
                driver.findElement(locatorToStopSearch);
                foundFlag = true;
            } catch (NoSuchElementException ignored) {
                logger.trace(marker, "Scrollig to coodinates [{},{}] - [{},{}]", startx, starty, endx, endy);
                new TouchAction(driver).press(startx, starty).moveTo(endx, endy).release().perform();
                try {Thread.sleep(150); } //wait for animation complete
                catch (InterruptedException ignored1) { }
            }
            if(count == -1) { if (foundFlag || System.currentTimeMillis() > endTime) break; }
            else { if (++counter == count) break; }
        }
        if (!foundFlag && count == -1) logger.warn(marker, "Final element on screen not found\nSearch locator: {}\nTimeout; {}", locatorToStopSearch, timeout);
        PageStore.put(new BoardPage()); //update page
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

    private By getStopLocator(ScrollDirection direction) {
        switch (direction) {
            case UP:
                return By.id("cardlist_header_container");

            case DOWN:
                return By.id("add_button");

            case RIGHT:
                try {
                    Field addListBtn = this.getClass().getDeclaredField("btnAddList");
                    FindBy findBy = addListBtn.getAnnotation(FindBy.class);
                    return By.id(findBy.id());
                } catch (NoSuchFieldException ignored) {}
            case LEFT:
                return By.xpath("//*[@resource-id='com.trello:id/list_name' and @text='FIRST']");

            default:
                throw new IllegalStateException("Unknown direction");
        }
    }

    @Override
    public String getTitle() {
        return titleContainer.getText();
    }

    public ListPage getListByIndex(int listIndex) {
        logger.debug(marker, "Finding list with index \"{}\"", listIndex);
        WebElement webElement = cardsLists.get(listIndex - 1);
        return new ListPage(webElement);
    }

    @Override
    public ListPage getListByName(String listName, SearchPolicy searchPolicy) {
        logger.debug(marker, "Finding list with name \"{}\"", listName);
        scroll(ScrollDirection.LEFT, -1);
        HashSet<String> set = new HashSet<>();
        int oldSetSize = set.size();
        do {
            List<ListPage> lists = getVisibleLists();
            for (ListPage list : lists) {
                String title = list.getTitle();
                logger.trace(marker, "Current list name: {}", title);
                if (searchPolicy.predicate().test(title, listName)) return list;
                set.add(title);
            }
            scroll(ScrollDirection.RIGHT, 1);
        } while (oldSetSize != set.size());
        throw new NotFoundException(String.format("List with name \"%s\" not found", listName));
    }


    public static class Menu {
        @FindBy(tagName = "android.widget.TextView")
        List<WebElement> menuEntries;

        private Menu() {
            Logger logger = LoggerFactory.getLogger(this.getClass());
            logger.debug(MarkerFactory.getMarker("PAGES?ANDROID"), "Initializing Board page menu");
            WebElement rootElement = Driver.get().findElement(By.id("drawer_container"));
            SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(rootElement);
            PageFactory.initElements(searchFactory, this);
        }
    }
}

package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.pages.annotations.Button;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MoveCardPage extends AbstractAndroidPage {
    @Button("Cancel")
    @FindBy(id = "button2")
    private WebElement btnCancel;

    @Button("Move")
    @FindBy(id = "button1")
    private WebElement btnOk;

    @Button("Board selector")
    @FindBy(id = "board_spinner")
    private WebElement boardSelector;

    @Button("List selector")
    @FindBy(id = "list_spinner")
    private WebElement listSelector;
}

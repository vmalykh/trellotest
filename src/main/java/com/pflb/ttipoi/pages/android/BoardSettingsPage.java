package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.interfaces.HasTitle;
import com.pflb.ttipoi.pages.annotations.Button;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BoardSettingsPage extends AbstractAndroidPage implements HasTitle {
    @Button("Rename")
    @FindBy(id = "title")
    WebElement title;

    public BoardSettingsPage() {
        logger.debug(marker, "Initializing Board settings page");
        WebElement searchContext = Driver.get().findElement(By.id("drawer_right"));
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
        PageFactory.initElements(searchFactory, this);
    }

    @Override
    public String getTitle() {
        return title.getText();
    }
}

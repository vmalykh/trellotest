package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.helpers.Config;
import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.repositories.PageStore;
import com.pflb.ttipoi.interfaces.Menu;
import com.pflb.ttipoi.interfaces.Scrollabe;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class MainSettingsPage extends AbstractAndroidPage implements Scrollabe, Menu {

    @FindBy(id = "title")
    private List<WebElement> entries;


    @Override
    public void scroll(ScrollDirection direction, int count) {
        logger.debug(marker, "Scrolling to {}, direction: {}", count == -1 ? "end of screen" : count + " pages", direction);
//        ScrollDirection actualDirection = direction.getActual();
        Dimension wndSize = Driver.get().manage().window().getSize();
        int startx = 0, starty = 0, endx = 0, endy = 0;
        By locatorToStopSearch = null;
        switch (direction) {
            case UP:
                starty = (int) Math.round(wndSize.height * 0.1);
                endy = (int) Math.round(wndSize.height * 0.9);
                startx = (int) Math.round(wndSize.width * 0.5);
//                endx = 0;
                locatorToStopSearch = By.xpath("//*[@text='Уведомления']");
                break;

            case DOWN:
                starty = (int) Math.round(wndSize.height * 0.9);
                endy = -starty;
                startx = (int) Math.round(wndSize.width * 0.5);
//                endx = 0;
                locatorToStopSearch = By.xpath("//*[@text='Выйти']");
        }
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS); //disable timeouts for search
        int timeout = Config.getGlobalTimeout();
        long endTime = System.currentTimeMillis() + timeout * 1000;
        boolean foundFlag = false;
        int counter = 0;

        if (locatorToStopSearch == null) throw new IllegalStateException("Locator to stop search not defined!");
        while (true) {
            try {//try to find button
                logger.trace(marker, "Trying to find last element on page");
                driver.findElement(locatorToStopSearch);
                foundFlag = true;
            } catch (NoSuchElementException ignored) {
                logger.trace(marker, "Scrollig to coodinates [{},{}] - [{},{}]", startx, starty, endx, endy);
                new TouchAction(driver).press(startx, starty).moveTo(endx, endy).release().perform();
                try {
                    Thread.sleep(150);
                } //wait for animation complete
                catch (InterruptedException ignored1) {
                }
            }
            if (count == -1) {
                if (foundFlag || System.currentTimeMillis() > endTime) break;
            } else {
                if (++counter == count) break;
            }
        }
        if (!foundFlag && count == -1)
            logger.warn(marker, "Final element on screen not found\nSearch locator: {}\nTimeout; {}", locatorToStopSearch, timeout);
        PageStore.put(new BoardPage()); //update page
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);

    }

    @Override
    public void selectMenuEntry(String text) {
        Optional<WebElement> entry = entries.stream().filter(e -> e.getText().equals(text)).findFirst();
        if (!entry.isPresent()) throw new NotFoundException("Cannot find element with text " + text);
        entry.get().click();
    }
}

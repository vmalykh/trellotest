package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.interfaces.HasTitle;
import com.pflb.ttipoi.interfaces.Selectable;
import com.pflb.ttipoi.interfaces.SmallCard;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SmallCardPage extends AbstractAndroidPage implements HasTitle, Selectable, SmallCard {
    private WebElement context;

    @FindBy(id = "cardText")
    private WebElement title;

    @FindBy(xpath = ".//*[@resource-id='com.trello:id/badgeContainer']/android.widget.LinearLayout")
    private List<WebElement> iconContainers;

    SmallCardPage(WebElement searchContext) {
        logger.debug(marker, "Initializing small card");
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
        PageFactory.initElements(searchFactory, this);
    }

    /**
     * Don't allow to invoke default constructor.
     */
    private SmallCardPage() {}

    /**
     * check for description icon is present on thi s card.
     * @return true if present
     */
    @Override
    public boolean hasDescriptionIcon() {
        logger.debug(marker, "Obtaining description icon on card \"{}\"", getTitle());
        if (iconContainers.size() == 0) return false;
        WebElement ic = iconContainers.get(0);
        try { WebElement element = ic.findElement(By.tagName("android.widget.TextView")); } //try to find checkList
        catch (NoSuchElementException ignored) {return true;}
        return false;
    }

    /**
     * check for checklist icon is present on thi s card.
     * @return true if present
     */
    @Override
    public boolean hasChecklistIcon() {
        logger.debug(marker, "Obtaining checklist icon on card \"{}\"", getTitle());
        if (iconContainers.size() == 0) return false;
        boolean foundFlag = false;
        for (WebElement ic : iconContainers) {
            try {
                ic.findElement(By.xpath(".//android.widget.TextView"));
                foundFlag = true;
            } catch (NoSuchElementException ignored) {}
        }
        return foundFlag;
    }

    /**
     * Select (tap) card.
     * Opens a FullCardPage
     */
    @Override
    public void select() {
        title.click();
    }

    /**
     * Geting checklists text container and returns values.
     * @return Array with 2 items;  [{checked},{total}]
     */
    @Override
    public int[] getCheckedCount() {
        logger.debug(marker, "Obtaining checklistchecked count on card \"{}\"", getTitle());
//        if (!hasChecklistIcon()) throw new IllegalStateException(String.format("Checklist dose'nt exist on card \"%s\"", getTitle()));
        if (iconContainers.size() == 0) throw new NotFoundException(String.format("Checklist does'nt exist on card \"%s\"", getTitle()));
        boolean foundFlag = false;
        String text = null;
        for (WebElement ic : iconContainers) {
            try {
                WebElement checkListTxtcontainer = ic.findElement(By.xpath(".//android.widget.TextView"));
                text = checkListTxtcontainer.getText();
                foundFlag = true;
            } catch (NoSuchElementException e) {
                logger.trace(marker, "TextView not found", e);
            }
        }
        if (!foundFlag) throw new NotFoundException(String.format("Checklist does'nt exist on card \"%s\"", getTitle()));
        int[] result = new int[2];
        String[] split = text.split("/");
        for (int i = 0; i < split.length; i++) {
            result[i] = Integer.valueOf(split[i]);
        }
        return result;
    }

    @Override
    public String getTitle() {
        return title.getText();
    }
}

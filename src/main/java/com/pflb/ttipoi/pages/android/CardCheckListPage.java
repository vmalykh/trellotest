package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.exceptions.InvalidOperationException;
import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.interfaces.CheckList;
import com.pflb.ttipoi.interfaces.Fillable;
import com.pflb.ttipoi.interfaces.HasTitle;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CardCheckListPage extends AbstractAndroidPage implements HasTitle, Fillable, CheckList {
    @FindBy(xpath = "//*[@resource-id='com.trello:id/expand_collapse_button']/../..")
    private WebElement head;

    @FindBy(xpath = "//*[@resource-id='com.trello:id/expand_collapse_button']/../../following-sibling::android.widget.FrameLayout[descendant::*[@resource-id='com.trello:id/name']]")
    private List<WebElement> items;

    CardCheckListPage(){}

    @Override
    public List<CheckList.Item> getItems() {
        List<CheckList.Item> result = new ArrayList<>();
        items.forEach(item -> result.add(new Item(item)));
        return result;
    }

    @Override
    public void setItemState(String elementName, boolean state) {
        //throw new RuntimeException("Not Implemented");
        Optional<Item> optionalItem = items.stream().map(Item::new).filter(item -> item.getTitle().equals(elementName)).findFirst();
        if (!optionalItem.isPresent()) throw new NotFoundException(String.format("Item with name \"%s\" not found on checklist \"%s\"", elementName, getTitle()));
        optionalItem.get().setChecked(state);
    }

    private Head getHead() {
        return new Head(head);
    }

    /**
     * checks if checklist exist (already created) on this card.
     * @return true if checklist created and exists
     */
    public boolean isExist() {
        try { return getHead().btnExpand.isDisplayed();}
        catch (NoSuchElementException ignored) { }
        return false;
    }

    public boolean isCollapsed() {
        return items.size() == 0;
    }

    /**
     * returns all checklist items.
     * @return java.utl.List with CardCheckListPage.Item's
     */

    @Override
    public String getTitle() {
        return getHead().getTitle();
    }

    @Override
    public void fill(String fieldName, String value) {
        logger.debug(marker, "Filling field \"{}\" with value \"{}\" on checklist \"{}\"", fieldName, value, getTitle());
        if (!fieldName.equals("Title")) throw new InvalidOperationException("Only title allowed to fill on CardCheckListPage");
        Head head = getHead();
        head.title.clear();
        head.title.sendKeys(value);
    }

    public static class Item implements CheckList.Item {
        @FindBy(id = "checkbox")
        private WebElement checkbox;

        @TextField("Item name")
        @FindBy(id = "name")
        private WebElement title;

        @Button("Delete")
        @FindBy(tagName = "android.widget.ImageButton")
        private WebElement btnDelete;

        private Item(WebElement searchContext) {
            SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
            PageFactory.initElements(searchFactory, this);
        }

        private void setChecked(boolean state) {
            if (Boolean.valueOf(checkbox.getAttribute("checked")) != state) checkbox.click();
        }

        public boolean isChecked() {
            return checkbox.isSelected();
        }

        @Override
        public String getTitle() {
            return title.getText();
        }
    }

    public static class Head implements HasTitle {
        @Button("Expand/Collapse")
        @FindBy(id = "expand_collapse_button")
        private WebElement btnExpand;

        @TextField("Title")
        @FindBy(id = "name")
        private WebElement title;

        private Head(WebElement searchContext) {
            SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
            PageFactory.initElements(searchFactory, this);
        }

        @Override
        public String getTitle() {
            return title.getText();
        }
    }
}

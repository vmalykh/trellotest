package com.pflb.ttipoi.pages.android;

public enum ScrollDirection {
    LEFT,
    RIGHT,
    UP,
    DOWN
}

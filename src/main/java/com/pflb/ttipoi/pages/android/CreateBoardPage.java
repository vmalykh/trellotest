package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.interfaces.Clickable;
import com.pflb.ttipoi.interfaces.Fillable;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateBoardPage extends AbstractAndroidPage implements Clickable, Fillable {
    @TextField("Board name")
    @FindBy(id = "com.trello:id/board_name")
    WebElement txtBoardName;

    @Button("Cancel")
    @FindBy(id = "android:id/button2")
    WebElement btnCancel;

    @Button("Ok")
    @FindBy(id = "android:id/button1")
    WebElement btnOk;
}

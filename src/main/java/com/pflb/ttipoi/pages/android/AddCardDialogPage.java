package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddCardDialogPage extends AbstractAndroidPage {
    @FindBy(id = "name")
    @TextField("Card name")
    private WebElement txtCardName;

    @FindBy(id = "description")
    @TextField("Card description")
    private WebElement txtDescription;

    @FindBy(id = "add_card")
    @Button("Add card")
    private WebElement btnConfirm;
}

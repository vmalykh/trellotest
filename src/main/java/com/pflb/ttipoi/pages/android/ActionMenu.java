package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.interfaces.Menu;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Optional;

public class ActionMenu extends AbstractAndroidPage implements Menu {
    @FindBy(xpath = "//android.widget.ListView//android.widget.TextView[@resource-id='com.trello:id/title']")
    private List<WebElement> entries;

    @Override
    public void selectMenuEntry(String text) {
        logger.debug(marker, "Selecting menu entry \"{}\"", text);
        Optional<WebElement> entry = entries.stream().filter(e -> e.getText().equals(text)).findFirst();
        if (!entry.isPresent()) throw new NotFoundException("Cannot find element with text " + text);
        entry.get().click();
    }
}

package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.interfaces.HasBoards;
import com.pflb.ttipoi.pages.annotations.Button;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public class MainPage extends AbstractAndroidPage implements HasBoards {
    @FindBy(id = "com.trello:id/primary_text")
    private WebElement userNameContainer;

    @Button("More")
    @FindBy(xpath = "//*[@resource-id='com.trello:id/toolbar']/android.support.v7.widget.LinearLayoutCompat/android.widget.ImageView")
    private WebElement moreMenuBtn;

    @FindBy(id = "com.trello:id/content")
    private WebElement content;

    @FindBy(xpath = "//*[@resource-id='com.trello:id/content']//*[@resource-id='com.trello:id/board_name']")
    private List<WebElement> boards;

    @Button("+")
    @FindBy(id = "com.trello:id/create_board")
    private WebElement btnCreate;

    /**
     * Finds all boards on page and returns his names.
     * @return List with name of each board
     */
    @Override
    public List<String> getAllBoardNames() {
        logger.debug(marker, "Getting all boards on main page");
        return boards.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    /**
     * Returns user name of current logged in user.
     * @return String with user name
     */
    public String getCurrentUserName() {
        return userNameContainer.getText();
    }

    /**
     * Selecting board by name.
     * @param boardName String with name of board to select
     */
    @Override
    public void selectBoard(String boardName) {
        logger.debug(marker, "Selecting board with name \"{}\"", boardName);
        boolean foundFlag = false;
        for (WebElement board : boards) {
            logger.trace(marker, "Current board: {}", board);
            if (!board.getText().equals(boardName)) continue;
            foundFlag = true;
            board.click();
            break;
        }
        if (!foundFlag) throw new NotFoundException(String.format("board with name \"%s\" can't be found on main page", boardName));
    }
}

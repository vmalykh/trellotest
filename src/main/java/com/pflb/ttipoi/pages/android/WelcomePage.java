package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.interfaces.Clickable;
import com.pflb.ttipoi.pages.annotations.Button;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WelcomePage extends AbstractAndroidPage implements Clickable {
    @FindBy(id = "com.trello:id/action_bar_root")
    private WebElement rootAppElement;

    @Button("Login")
    @FindBy(id = "com.trello:id/log_in_button")
    private WebElement btnLogin;

    public void waitForPresence() {
        wait.until(d -> (rootAppElement.isDisplayed()));
    }
}

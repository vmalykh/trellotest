package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.interfaces.CheckList;
import com.pflb.ttipoi.interfaces.HasCheckList;
import com.pflb.ttipoi.interfaces.HasTitle;
import com.pflb.ttipoi.pages.annotations.Button;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FullCardPage extends AbstractAndroidPage implements HasTitle, HasCheckList {
    @Button("Rename")
    @TextField("Title")
    @FindBy(id = "card_name")
    private WebElement title;

    @Button("Confirm")
    @FindBy(id = "confirm")
    private WebElement btnConfirm;

    @Button("Cancel")
    @FindBy(xpath = "//*[@resource-id='com.trello:id/editing_toolbar']/android.widget.ImageButton")
    private WebElement btnCancel;

    @Button("Back")
    @FindBy(xpath = "//*[@resource-id='com.trello:id/toolbar']/android.widget.ImageButton")
    private WebElement btnback;

    @TextField("Description")
    @FindBy(id = "card_description")
    private WebElement cardDescription;

    @Button("+")
    @FindBy(id = "fab_expand_menu_button")
    private WebElement btnAdd;

    @Button("Add checklist")
    @FindBy(xpath = "//*[@resource-id='com.trello:id/card_fab']/android.widget.TextView[1]")
    private WebElement btnAddChecklist;

    @Button("Add attach")
    @FindBy(xpath = "//*[@resource-id='com.trello:id/card_fab']/android.widget.TextView[1]")
    private WebElement btnAddAttach;

    @TextField("New checklist item")
    @FindBy(id = "add_checkitem_edit_text")
    private WebElement txtNewChecklistItem;

    @Button("More")
    @FindBy(xpath = "//*[@resource-id='com.trello:id/toolbar']//android.widget.ImageView")
    private WebElement btnMenu;

    @FindBy(id = "card_cover")
    private WebElement cardCover;

////    @FindBy(xpath = "//*[@resource-id='com.trello:id/expand_collapse_button']/../..")
//    @FindBy(id = "list")
//    private WebElement checkListContext;


    public FullCardPage() {
        super();
        wait.until(d -> cardCover.isDisplayed());
    }


    @Override
    public CheckList getCheckList() {
        return new CardCheckListPage();
    }

    @Override
    public boolean hasCheckList() {
        try { return driver.findElementById("expand_collapse_button").isDisplayed(); }
        catch (NoSuchElementException ignored) {}
        return false;
    }

    @Override
    public String getTitle() {
        return title.getText();
    }
}

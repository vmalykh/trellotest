package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.SearchContextElementLocatorFactory;
import com.pflb.ttipoi.interfaces.Menu;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.List;
import java.util.Optional;

public class RightSideMenu implements Menu {
    Logger logger = LoggerFactory.getLogger(getClass());
    Marker marker = MarkerFactory.getMarker("PAGES/ANDROID");

    @FindBy(xpath = ".//android.widget.TextView")
    private List<WebElement> entries;

    public RightSideMenu() {
        logger.debug(marker, "Initializing menu Right side menu");
        WebElement searchContext = Driver.get().findElement(By.id("drawer_right"));
        SearchContextElementLocatorFactory searchFactory = new SearchContextElementLocatorFactory(searchContext);
        PageFactory.initElements(searchFactory, this);
    }

    @Override
    public void selectMenuEntry(String text) {
        logger.debug(marker, "Selecting menu entry \"{}\"", text);
        Optional<WebElement> entry = entries.stream().filter(e -> e.getText().equals(text)).findFirst();
        if (!entry.isPresent()) throw new NotFoundException("Cannot find element with text " + text);
        entry.get().click();
    }
}

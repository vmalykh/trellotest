package com.pflb.ttipoi.pages.android;

import com.pflb.ttipoi.pages.AbstractPage;
import io.appium.java_client.android.AndroidDriver;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public abstract class AbstractAndroidPage extends AbstractPage {
    protected Marker marker = MarkerFactory.getMarker("PAGES/ANDROID");
    protected AndroidDriver driver = (AndroidDriver) super.driver;
}

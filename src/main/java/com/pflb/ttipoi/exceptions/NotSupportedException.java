package com.pflb.ttipoi.exceptions;

public class NotSupportedException extends RuntimeException {

    public NotSupportedException() {
        super();
    }

    public NotSupportedException(String message) {
        super(message);
    }

    public NotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }
}

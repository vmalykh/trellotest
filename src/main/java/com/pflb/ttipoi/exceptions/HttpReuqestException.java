package com.pflb.ttipoi.exceptions;

public class HttpReuqestException extends RuntimeException {
    public HttpReuqestException() {
    }

    public HttpReuqestException(String message) {
        super(message);
    }

    public HttpReuqestException(String message, Throwable cause) {
        super(message, cause);
    }
}

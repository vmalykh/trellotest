package com.pflb.ttipoi.exceptions;

public class DriverNotSupportedException extends RuntimeException {
    public DriverNotSupportedException(String message) {
        super(message);
    }

    public DriverNotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }
}

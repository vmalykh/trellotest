package com.pflb.ttipoi.exceptions;

public class ConfigurationInvalidException extends RuntimeException {
    public ConfigurationInvalidException() {
    }

    public ConfigurationInvalidException(String message) {
        super(message);
    }

    public ConfigurationInvalidException(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.pflb.ttipoi.models;

import io.restassured.path.json.JsonPath;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Card extends AbstractModel {
    public String id;
    public String name;
    public String desc;

    private static final String SAVE_PATH_TPL = "/cards/%s";
    private static final String ADD_CHECKLIST_PATH = "/checklists";
    private static final String GET_CHECKLISTS_PATH = "/cards/%s/checklists";

    public Card(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public void save() {
        if (id == null) throw new IllegalStateException("Cannot save card, id required but null");
        logger.debug(marker, "Saving card {}", name);
        String savePath = String.format(SAVE_PATH_TPL, id);
        ExtractableResponse<Response> response = getRequest().queryParam("name", name).queryParam("desc", desc)
                .put(savePath).then().extract();
        checkStatusCode(response.statusCode());
    }

    public void move(com.pflb.ttipoi.models.List list) {
        if (id == null) throw new IllegalStateException("Cannot move card, id required but null");
        if (list.id == null) throw new IllegalStateException("Cannot move card, list id required but null");
        logger.debug(marker, "Moving card \"{}\" to list \"{}\"", this.name, list.name);
        String cardSavePath = String.format(SAVE_PATH_TPL, id);
        ExtractableResponse<Response> response = getRequest().queryParam("idList", list.id)
                .put(cardSavePath).then().extract();
        checkStatusCode(response.statusCode());
    }

    public void delete() {
        if (id == null) throw new IllegalStateException("Cannot delete card, id required but null");
        logger.debug(marker, "Deleting card \"{}\"", name);
        String cardSavePath = String.format(SAVE_PATH_TPL, id);
        int statusCode = getRequest().delete(cardSavePath).then().extract().statusCode();
        checkStatusCode(statusCode);
        id = null;
    }

    public CheckList addCheckList(String name) {
        if (id == null) throw new IllegalStateException("Cannot add checklist, id required but null");
        logger.debug(marker, "Adding checklist \"{}\" to card \"{}\'", name, this.name);
        ExtractableResponse<Response> response = getRequest().queryParams("name", name).queryParam("idCard", id)
                .post(ADD_CHECKLIST_PATH).then().extract();
        checkStatusCode(response.statusCode());
        JsonPath json = response.jsonPath();
        return new CheckList(json.getString("id"), this, json.getString("name"));
    }

    public List<CheckList> getChecklists() {
        if (id == null) throw new IllegalStateException("Cannot get checklists, id required but null");
        logger.debug(marker, "Getting all checklists on card \"{}\"", name);
        String getChecklistsPath = String.format(GET_CHECKLISTS_PATH, id);
        ExtractableResponse<Response> response = getRequest().get(getChecklistsPath).then().extract();
        checkStatusCode(response.statusCode());
//        return response.jsonPath().getList("", CheckList.class);
        List<Map<String, String>> maps = response.jsonPath().getList("");
        List<CheckList> result = new ArrayList<>();
        maps.forEach(map -> result.add(new CheckList(map.get("id"), this, map.get("name"))));
        return result;
    }
}

package com.pflb.ttipoi.models;

import io.restassured.path.json.JsonPath;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckList extends AbstractModel {
    private Card card;
    public String id;
    public String name;

    private static final String CHECKITEM_PATH_TPL = "/checklists/%s/checkItems";
    private static final String CHECKLIST_SAVE_PATH_TPL = "/checklists/%s";

    CheckList(String id, Card card, String name) {
        this.id = id;
        this.name = name;
        this.card = card;
    }

    public CheckItem addCheckItem(String name, Boolean state) {
        if (id == null) throw new IllegalStateException("Cannot add checkitem, id required but null");
        if (name == null) throw new IllegalStateException("Cannot add checkitem, checkitem name required but null");
        String addCheckitemPath = String.format(CHECKITEM_PATH_TPL, id);
        logger.debug(marker, "Adding checklist item \"{}\" to checkist \"{}\"", name, name);
        RequestSpecification localRequest = getRequest().queryParams("name", name).queryParam("id", id);
        if (state != null) localRequest.queryParam("checked", state);
        ExtractableResponse<Response> response = localRequest.post(addCheckitemPath).then().extract();
        checkStatusCode(response.statusCode());
        JsonPath json = response.body().jsonPath();
        return new CheckItem(json.getString("id"), card, this, json.getString("name"), json.getString("state").equals("complete"));

    }

    public List<CheckItem> getCheckItems() {
        if (id == null) throw new IllegalStateException("Cannot get checkitems, id required but null");
        String getCheckitemsPath = String.format(CHECKITEM_PATH_TPL, id);
        logger.debug(marker, "Gettig all checkitems of checklist \"{}\"", name);
        List<Map<String, String>> list = getRequest().get(getCheckitemsPath).then().extract().body().jsonPath().<Map<String, String>>getList("");
        ArrayList<CheckItem> checkItems = new ArrayList<>();
        list.forEach(m -> checkItems.add(new CheckItem(m.get("id"), card, this, m.get("name"), m.get("state").equals("complete"))));
        return checkItems;
    }

    public void save() {
        if (id == null) throw new IllegalStateException("Cannot save checklist, id required but null");
        logger.debug(marker, "Saving checklist {}", name);
        String saveChecklistPath = String.format(CHECKLIST_SAVE_PATH_TPL, id);
        ExtractableResponse<Response> response = getRequest().queryParams(toMap()).queryParams("id", this.id)
                .put(saveChecklistPath).then().extract();
        checkStatusCode(response.statusCode());
    }

    public static class CheckItem extends AbstractModel {
        private Card card;
        private CheckList checkList;
        public String id;
        public boolean state;
        public String name;

        private static final String CHECKITEM_SAVE_PATH_TPL = "/cards/%s/checkItem/%s";

        private CheckItem(String id, Card card, CheckList checkList, String name, boolean state) {
            this.id = id;
            this.state = state;
            this.name = name;
            this.card = card;
            this.checkList = checkList;
        }

        public void save() {
            if (id == null) throw new IllegalStateException("Cannot save checklist, id required but null");
            logger.debug(marker, "Saving checklist {}", name);
            String checkItemSavePath = String.format(CHECKITEM_SAVE_PATH_TPL, card.id, id);
            ExtractableResponse<Response> response = getRequest().queryParams(toMap())
                    .put(checkItemSavePath).then().extract();
            checkStatusCode(response.statusCode());
        }

        @Override
        public Map<String, Object> toMap() {
            return new HashMap<String, Object>() {{
                put("name", name);
                put("state", state ? "complete" : "incomplete");
            }};
        }
    }
}

package com.pflb.ttipoi.models;

import com.google.gson.GsonBuilder;
import com.pflb.ttipoi.exceptions.HttpReuqestException;
import com.pflb.ttipoi.helpers.Config;
import com.pflb.ttipoi.helpers.LoggerPrintStream;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.filter.log.LogDetail;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import static com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;

public abstract class AbstractModel {
    protected static Logger logger = LoggerFactory.getLogger(new Object(){}.getClass().getEnclosingClass());
    protected static Marker marker = MarkerFactory.getMarker("API/MODEL");

    protected static RequestSpecification getRequest() {
        RestAssured.config = RestAssured.config()
                .objectMapperConfig(new ObjectMapperConfig(ObjectMapperType.GSON).gsonObjectMapperFactory(
                        (type, s) -> new GsonBuilder().setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES).create())
                )
                .logConfig(new LogConfig().defaultStream(new LoggerPrintStream(logger, marker)));

        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setBaseUri(Config.getApiUrl())
                .addHeader("Content-Type", "application/json")
                .addQueryParam("key", Config.getApiKey())
                .addQueryParam("token", Config.getApiToken())
                .log(LogDetail.ALL);
        RequestSpecification spec = builder.build();
        return RestAssured.given().spec(spec);
    }

    protected static void checkStatusCode(int actualStatusCode, int expectedStatuscode) {
        if (actualStatusCode != expectedStatuscode) throw new HttpReuqestException(String.format("Unexpected status code. Expected: %d, Actual: %d", expectedStatuscode, actualStatusCode));
    }

    protected static void checkStatusCode(int actualStatusCode) {
        checkStatusCode(actualStatusCode, 200);
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        Field[] fields = this.getClass().getDeclaredFields();
        try {
            for (Field field : fields) {
                if ((Modifier.FINAL & field.getModifiers()) != 0 || (Modifier.PRIVATE & field.getModifiers()) != 0 || field.getName().equals("id")) continue; //don't serialize fields with final, private modifier and id;
                Object value = field.get(this);
                result.put(field.getName(), value);
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return result;
    }
}

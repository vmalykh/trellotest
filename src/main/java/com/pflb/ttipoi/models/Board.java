package com.pflb.ttipoi.models;

import io.restassured.path.json.JsonPath;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.openqa.selenium.NotFoundException;

import java.util.List;
import java.util.Optional;

public class Board extends AbstractModel {
    public String name;
    public String id;
    public String url;

    private static final String CREATE_BOARD_PATH = "/boards";
    private static final String UPDATE_BOARD_PATH_TPL = "/boards/%s";
    private static final String BOARDS_LIST_PATH = "/members/me/boards";
    private static final String LISTS_LIST_PATH_TPL = "/boards/%s/lists";
    private static final String CREATE_LIST_PATH = "/list";

    public Board(String name) {
        this.name = name;
    }
    public Board(){}

    public void save() {
        if (id == null) post();
        else put();
    }

    private void post() {
        if (name == null) throw new IllegalStateException("Cannot save board, name required but null");
        logger.debug(marker, "Creating board. Name \"{}\"", name);
//        RequestSpecification localRequest = getRequest().when().queryParam("name", this.name);
        RequestSpecification localRequest = getRequest().when().queryParams(toMap());
        ExtractableResponse<Response> response = localRequest.post(CREATE_BOARD_PATH).then().extract();
        checkStatusCode(response.statusCode());
        JsonPath json = response.body().jsonPath();
        this.id = json.getString("id");
        this.name = json.getString("name");
        this.url = json.getString("url");
    }

    private void put() {
        if (id == null) throw new IllegalStateException("Cannot save board, id required but null");
        if (name != null) getRequest().queryParam("name", name);
        String updateBoardPath = String.format(UPDATE_BOARD_PATH_TPL, id);
        ExtractableResponse<Response> response = getRequest().queryParam("name", name)
                .put(updateBoardPath).then().extract();
        checkStatusCode(response.statusCode());
    }

    public static List<Board> getAllBoards() {
        logger.debug(marker, "Requesting all boards list");
        ExtractableResponse<Response> response = getRequest().get(BOARDS_LIST_PATH).then().extract();
        checkStatusCode(response.statusCode());
        return response.body().jsonPath().getList("", Board.class);
    }

    public void createList(com.pflb.ttipoi.models.List list) {
        if (list.name == null) throw new IllegalStateException("Cannot save list, name required but null");
        if (id == null) throw new IllegalStateException("Cannot save list, board id required but null");
        logger.debug(marker, "Saving new list with name \"{}\" on board \"{]\"", list.name, name);
        ExtractableResponse<Response> response = getRequest().queryParam("name", list.name).queryParam("idBoard", id)
                .post(CREATE_LIST_PATH).then().extract();
        checkStatusCode(response.statusCode());
        list.id = response.body().jsonPath().getString("id");
    }

    public List<com.pflb.ttipoi.models.List> getLists() {
        if (id == null) throw new IllegalStateException("Cannot load lists, board id required but null");
        logger.debug(marker, "Requesting all lists on board {}", name);
        String listsListPath = String.format(LISTS_LIST_PATH_TPL, id);
        ExtractableResponse<Response> response = getRequest().get(listsListPath).then().extract();
        checkStatusCode(response.statusCode());
        return response.body().jsonPath().getList("", com.pflb.ttipoi.models.List.class);
    }

    public com.pflb.ttipoi.models.List getListByNameStartsWith(String listName) {
        logger.debug(marker, "Requesting list by name stats with {} on board {}", listName, name);
        List<com.pflb.ttipoi.models.List> lists = getLists();
        Optional<com.pflb.ttipoi.models.List> optionalList = lists.stream().filter(list -> list.name.startsWith(listName)).findFirst();
        if (!optionalList.isPresent()) throw new NotFoundException(String.format("List with name starts with \"%s\" not found on board \"%s\"", listName, name));
        return optionalList.get();
    }

    public void remove() {
        if (id == null) throw new IllegalStateException("Cannot remove, board id required but null");
        logger.debug(marker, "Removing board \"{}\"", name);
        String deleteBoardPath = String.format(UPDATE_BOARD_PATH_TPL, id);
        ExtractableResponse<Response> response = getRequest().delete(deleteBoardPath).then().extract();
        checkStatusCode(response.statusCode());
    }
}

package com.pflb.ttipoi.models;

import io.restassured.path.json.JsonPath;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.openqa.selenium.NotFoundException;

import java.util.Optional;

public class List extends AbstractModel {
    public String id;
    public String name;

    private static final String LIST_SAVE_PATH_TPL = "/lists/%s";
    private static final String CARDS_LIST_PATH_TPL = "/lists/%s/cards";
    private static final String CREATE_CARD_PATH = "/cards";

    public List(String name) {
        this.name = name;
    }

    public void rename() {
        if (id == null) throw new IllegalStateException("Cannot rename list: id required but null");
        logger.debug(marker, "Updating (rename) list \"{}\"", name);
        String listSavePath = String.format(LIST_SAVE_PATH_TPL, id);
        int statusCode = getRequest().queryParam("name", this.name).put(listSavePath).then().extract().statusCode();
        checkStatusCode(statusCode);
    }

    public void move(Board board) {
        if (id == null) throw new IllegalStateException("Cannot move list: list id required but null");
        if (board.id == null) throw new IllegalStateException("Cannot rename list: board id required but null");
        logger.debug("Moving list \"{}\" to board \"{}\"", name, board.name);
        String listSavePath = String.format(LIST_SAVE_PATH_TPL, id);
        int statusCode = getRequest().queryParam("idBoard", board.id).put(listSavePath).then().extract().statusCode();
        checkStatusCode(statusCode);
    }

    public void createCard(Card card) {
        if (id == null) throw new IllegalStateException("Cannot create new card, list id required but null");
        logger.debug(marker, "Creating card \"{}\"", card.name);
        RequestSpecification localRequest = getRequest().queryParam("idList", id);
        if (card.name != null) localRequest.queryParam("name", card.name);
        if (card.desc != null) localRequest.queryParam("desc", card.desc);
        ExtractableResponse<Response> response = localRequest.post(CREATE_CARD_PATH).then().extract();
        checkStatusCode(response.statusCode());
        JsonPath json = response.body().jsonPath();
        card.id = json.get("id");
        card.name = json.get("name");
        card.desc = json.get("desc");
    }

    public java.util.List<Card> getCards() {
        if (id == null) throw new IllegalStateException("Cannot get cards, list id required but null");
        logger.debug(marker, "Requesting all cards in list {}", name);
        String cardListPath = String.format(CARDS_LIST_PATH_TPL, id);
        ExtractableResponse<Response> response = getRequest().get(cardListPath).then().extract();
        checkStatusCode(response.statusCode());
        return response.body().jsonPath().getList("", Card.class);
    }

    public Card getCardByNameStartsWith(String cardName) {
        logger.debug(marker, "Requesting card by name stats with {} in list {}", cardName, name);
        java.util.List<Card> cards = getCards();
        Optional<Card> optionalCard = cards.stream().filter(card -> card.name.startsWith(cardName)).findFirst();
        if (!optionalCard.isPresent()) throw new NotFoundException(String.format("Card with name starts with \"%s\" not found in list \"%s\"", cardName, name));
        return optionalCard.get();
    }
}

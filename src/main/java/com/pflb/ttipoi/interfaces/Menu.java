package com.pflb.ttipoi.interfaces;

public interface Menu {
    void selectMenuEntry(String text);

//    static Menu get() {
//        Map<By, Class<? extends Menu>> menus = new HashMap<>();
//        menus.put(By.xpath("//android.widget.ListView//android.widget.TextView[@resource-id='com.trello:id/title']"), ActionMenuTypeOne.class);
//        menus.put(By.xpath("/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView"), ActionMenuTypeTwo.class);
//
//        AtomicReference<Class<? extends Menu>> resultStore = new AtomicReference<>();
//        WebDriver driver = Driver.get();
//        menus.forEach((locator, klass) -> {
//            try {
//                driver.findElement(locator);
//                resultStore.set(klass);
//            }
//            catch (NoSuchElementException ignored) { }
//        });
//        if (resultStore.get() == null) throw new NotFoundException("Cannot find any known menu");
//        Class<? extends Menu> klass = resultStore.get();
//        try {
//            Constructor<? extends Menu> constructor = klass.getDeclaredConstructor();
//            return constructor.newInstance();
//        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
//            throw new RuntimeException(e);
//        }
//    }
}

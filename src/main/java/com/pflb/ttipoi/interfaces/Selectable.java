package com.pflb.ttipoi.interfaces;

public interface Selectable {
    void select();
}

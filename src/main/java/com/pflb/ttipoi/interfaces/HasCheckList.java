package com.pflb.ttipoi.interfaces;

public interface HasCheckList {
    boolean hasCheckList();
    CheckList getCheckList();
}

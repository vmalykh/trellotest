package com.pflb.ttipoi.interfaces;


import com.pflb.ttipoi.helpers.SearchPolicy;

import java.util.List;

public interface HasCards extends HasTitle {
    List<SmallCard> getCards();
    SmallCard getCardByName(String cardName, SearchPolicy searchPolicy);
}

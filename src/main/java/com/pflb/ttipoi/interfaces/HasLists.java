package com.pflb.ttipoi.interfaces;

import com.pflb.ttipoi.helpers.SearchPolicy;

public interface HasLists extends HasTitle {
    HasCards getListByName(String name, SearchPolicy searchPolicy);
}

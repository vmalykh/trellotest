package com.pflb.ttipoi.interfaces;

public interface Clickable {
    void click(String buttonName);
}

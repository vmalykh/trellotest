package com.pflb.ttipoi.interfaces;

import com.pflb.ttipoi.pages.android.ScrollDirection;

public interface Scrollabe {
    void scroll(ScrollDirection direction, int countOfPages);
}

package com.pflb.ttipoi.interfaces;

import java.util.List;

public interface CheckList extends HasTitle {
    List<Item> getItems();

    interface Item extends HasTitle  {
//        void setChecked(boolean state);
    }

    void setItemState(String elementName, boolean state);
}

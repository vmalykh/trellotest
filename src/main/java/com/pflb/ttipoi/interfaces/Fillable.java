package com.pflb.ttipoi.interfaces;

import com.pflb.ttipoi.exceptions.InvalidOperationException;
import com.pflb.ttipoi.pages.annotations.TextField;
import org.slf4j.LoggerFactory;

import org.slf4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

public interface Fillable {
//    void fill(String fieldName, String value);
    default void fill(String fieldName, String value) {
        Logger logger = LoggerFactory.getLogger(this.getClass());
        logger.debug("Filling \"{}\" with value \"{}\"", fieldName, value);
        Class<?> klass = this.getClass();
        Field[] fields = klass.getDeclaredFields();
        Optional<Field> optionalField;
        optionalField = Arrays.stream(fields)
                .filter(f -> f.isAnnotationPresent(TextField.class) && f.getAnnotation(TextField.class).value().equals(fieldName))
                .findFirst();
        if (!optionalField.isPresent()) throw new InvalidOperationException(String.format("Field annotated @TextField with name \"%s\" can't be found in class \"%s\"",fieldName, this.getClass().getSimpleName()));
        Field field = optionalField.get();
        field.setAccessible(true);
        try {
            Object fieldValue = field.get(this);
            Method skMethod = fieldValue.getClass().getMethod("sendKeys", CharSequence[].class);
            Method clearMethod = fieldValue.getClass().getMethod("clear");
            CharSequence[] vti = new String[]{value};
            clearMethod.invoke(fieldValue);
            skMethod.invoke(fieldValue, new Object[]{vti});
        }
        catch (IllegalAccessException e) { throw new RuntimeException("Cannot get access to field", e); }
        catch (NoSuchMethodException e) { throw new RuntimeException("Cannot find method", e); }
        catch (InvocationTargetException e) { throw new RuntimeException("Cannot execute method", e); }
    }
}

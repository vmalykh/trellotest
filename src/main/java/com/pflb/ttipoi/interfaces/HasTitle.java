package com.pflb.ttipoi.interfaces;

public interface HasTitle {
    String getTitle();
}

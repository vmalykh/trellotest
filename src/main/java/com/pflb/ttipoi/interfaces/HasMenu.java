package com.pflb.ttipoi.interfaces;

public interface HasMenu {
    void invokeMenu();
}

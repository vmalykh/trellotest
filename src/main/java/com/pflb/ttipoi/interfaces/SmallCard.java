package com.pflb.ttipoi.interfaces;

public interface SmallCard {
    boolean hasDescriptionIcon();
    boolean hasChecklistIcon();
    String getTitle();
    int[] getCheckedCount();

}

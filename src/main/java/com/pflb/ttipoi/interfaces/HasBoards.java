package com.pflb.ttipoi.interfaces;


import java.util.List;

public interface HasBoards {
    List<String> getAllBoardNames();
    void selectBoard(String name);
}

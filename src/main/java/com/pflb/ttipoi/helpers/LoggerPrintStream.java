package com.pflb.ttipoi.helpers;

import org.slf4j.Logger;
import org.slf4j.Marker;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class LoggerPrintStream extends PrintStream {
    private static PrintStream ps;

    public LoggerPrintStream(Logger logger, Marker marker) {
        super(getPrintStream(logger, marker));
    }

    private static PrintStream getPrintStream(Logger logger, Marker marker) {
        if (ps == null) {
            OutputStream outputStream = new OutputStream() {
                private StringBuilder sb = new StringBuilder();

                @Override
                public void write(int b) throws IOException {
                    sb.append((char)b);
                }

                @Override
                public void flush() throws IOException {
                    logger.trace(marker, this.sb.toString());
                    sb = new StringBuilder();
                }
            };
            ps = new PrintStream(outputStream, true);
        }
        return ps;
    }
}

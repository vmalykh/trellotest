package com.pflb.ttipoi.helpers;

import com.pflb.ttipoi.exceptions.DriverNotSupportedException;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public class Driver {
    private static ThreadLocal<WebDriver> driverStore = new ThreadLocal<>();
    private static Logger logger = LoggerFactory.getLogger(Driver.class);
    private static Marker marker = MarkerFactory.getMarker("DRIVER");
    public synchronized static WebDriver forName(String name) {
        logger.debug(marker, "Obtaining driver for {}", name);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        RemoteWebDriver driver;
        switch (name) {
            case "firefox":
                capabilities.setCapability("browserName", "firefox");
                capabilities.setCapability("version", "");
                capabilities.setCapability("platform", Platform.ANY);
                driver = new RemoteWebDriver(Config.getSeleniumHubUrl(), capabilities);
                break;

            case "chrome":
                capabilities.setCapability("browserName", "chrome");
                capabilities.setCapability("version", "");
                capabilities.setCapability("platform", Platform.ANY);
                driver = new RemoteWebDriver(Config.getSeleniumHubUrl(), capabilities);
                driver.manage().window().maximize();

                break;

            case "android":
                capabilities.setCapability("platformName", Platform.ANDROID);
                capabilities.setCapability("deviceName", "Android");
                capabilities.setCapability("appPackage", Config.getAppPakage());
                capabilities.setCapability("appActivity", Config.getAppActivity());
                capabilities.setCapability("version", "");
                capabilities.setCapability("browserName", "Android");
                driver = new AndroidDriver<>(Config.getSeleniumHubUrl(), capabilities);
                break;

            default:
                throw new DriverNotSupportedException(String.format("Driver for given name \"%s\" not supported", name));
        }
        driverStore.set(driver);
        return driver;
    }

    /**
     * Get driver from thead-local storage and return
     * @return driver from thread-local storage
     */
    public synchronized static WebDriver get() {
        return driverStore.get();
    }
}

package com.pflb.ttipoi.helpers;

import com.pflb.ttipoi.exceptions.ConfigurationInvalidException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class Credentials extends Properties {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    Marker marker = MarkerFactory.getMarker("CONFIG/CRED");

    public Credentials() {
        InputStream resource = getClass().getClassLoader().getResourceAsStream("credentials.properties");
        if (resource != null) {
            try { load(resource); }
            catch (IOException e) { throw new RuntimeException("File \"credentials.properties\" found but not readable", e); }
            return;
        }
        logger.debug(marker, "Credentials was not found in classpath, trying to find path in $CREDS");
        String path = System.getenv("CREDS");
        logger.trace(marker, "CREDS path: {}", path);
        File file = new File(path);
        if (file.exists()) readEnvFromFile(file);
        else readEnvFromEnvVars();
    }

    private void readEnvFromFile(File file) {
        logger.debug(marker, "Reading credentials from environment variables");
        try {
            FileInputStream inputStream = new FileInputStream(file);
            load(inputStream);
        }
        catch (IOException e) {
            throw new RuntimeException("Loading credentials failed", e);
        }
        logger.trace(marker, "user: {}", getProperty("credentials.username"));
        logger.trace(marker, "token: {}", getProperty("api.token"));
    }

    private void readEnvFromEnvVars() {
        List<String> names = new LinkedList<String>() {{
            add("credentials.username");
            add("credentials.email");
            add("credentials.password");
            add("api.key");
            add("api.token");
        }};
        for (String name : names) {
            String value = System.getenv(name);
            if (value == null || value.equals("")) {
                RuntimeException cause = new RuntimeException(String.format("Variable \"%s\" required but not found", name));
                throw new ConfigurationInvalidException("Cannot find credentials, unable to continue. See readme.md for more informantion", cause);
            }
        }
    }
}

package com.pflb.ttipoi.helpers;

import java.util.function.BiPredicate;

public enum SearchPolicy {
    EQUALS {
        public BiPredicate<String, String> predicate() { return Object::equals; }
    },
    STARTS_WITH {
        public BiPredicate<String, String> predicate() { return String::startsWith; }
    },
    CONTAINS {
        public BiPredicate<String, String> predicate() { return String::contains; }
    };

    public abstract BiPredicate<String, String> predicate();
}

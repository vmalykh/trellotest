package com.pflb.ttipoi.helpers;

import com.pflb.ttipoi.helpers.repositories.Storage;

import java.util.UUID;

public class Utils {
    public static String uuid() {
        return UUID.randomUUID().toString();
    }

    public static String getName(String key) {
        if (!key.startsWith("Storage => ")) return key;
        String storageKey = key.split("Storage => ")[1];
        return Storage.get(storageKey).toString();
    }
}

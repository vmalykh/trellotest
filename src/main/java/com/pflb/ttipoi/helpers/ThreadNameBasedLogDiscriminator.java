package com.pflb.ttipoi.helpers;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.sift.Discriminator;

public class ThreadNameBasedLogDiscriminator implements Discriminator<ILoggingEvent> {
    private final String KEY = "threadName";
    private boolean startFlag;

    @Override
    public String getDiscriminatingValue(ILoggingEvent iLoggingEvent) {
        return Thread.currentThread().getName();
    }

    @Override
    public String getKey() {
        return KEY;
    }

    @Override
    public void start() {
        startFlag = true;
    }

    @Override
    public void stop() {
        startFlag = false;
    }

    @Override
    public boolean isStarted() {
        return startFlag;
    }
}

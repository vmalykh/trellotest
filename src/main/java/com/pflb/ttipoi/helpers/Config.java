package com.pflb.ttipoi.helpers;

import com.pflb.ttipoi.exceptions.ConfigurationInvalidException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

/**
 * Read all config parameters
 */
public class Config {
//    private static Config instance;
    private static final Logger logger = LoggerFactory.getLogger(Config.class);
    private static final Marker marker = MarkerFactory.getMarker("CONFIG");
    private static final String CONFIG_FILE_NAME = "config.properties";
    private static final String CREDENTIALS_FILE_NAME = "credentials.properties";

    @Property(name = "web.url.base", source = Source.CONFIG)
    private static URL baseUrl;

    @Property(name = "api.url.base", source = Source.CONFIG)
    private static URI apiUrl;

    @Property(name = "credentials.username", source = Source.CREDENTIALS)
    private static String username;

    @Property(name = "credentials.email", source = Source.CREDENTIALS)
    private static String email;

    @Property(name = "credentials.password", source = Source.CREDENTIALS)
    private static String password;

    @Property(name = "api.key", source = Source.CREDENTIALS)
    private static String apiKey;

    @Property(name = "api.token", source = Source.CREDENTIALS)
    private static String apiToken;

    @Property(name = "app.package", source = Source.CONFIG)
    private static String appPakage;

    @Property(name = "app.home_activity", source = Source.CONFIG)
    private static String appActivity;

    @Property(name = "selenium.hub.url", source = Source.CONFIG)
    private static URL seleniumHubUrl;

    @Property(name = "global.timeout", source = Source.CONFIG)
    private static int globalTimeout;


//    static {
//        logger.debug(marker, "Loading configuration");
//        InputStream configInpuStream = Config.class.getClassLoader().getResourceAsStream(CONFIG_FILE_NAME);
//        InputStream credentialsInputStream = Config.class.getClassLoader().getResourceAsStream(CREDENTIALS_FILE_NAME);
//        Properties configProperties = new Properties();
//        Properties credentialsProperties = new Properties();
//        logger.debug("Parsing properties");
//        try {
//            configProperties.load(configInpuStream);
//            credentialsProperties.load(credentialsInputStream);
//            baseUrl = new URL(configProperties.getProperty("web.url.base"));
//            apiUrl = new URL(configProperties.getProperty("api.url.base"));
//        } catch (MalformedURLException e) {
//            logger.error(marker, "Unable to pasre url", e);
//            throw new IllegalStateException("Unable to Unable to pasre url", e);
//        } catch (IOException e) {
//            logger.error(marker, "Unable to load configuration file", e);
//            throw new IllegalStateException("Unable to load configuration file", e);
//        }
//        username = credentialsProperties.getProperty("credentials.username");
//        password = credentialsProperties.getProperty("credentials.password");
//
//    }

    static {
        Class<Config> klass = Config.class;
        Field[] fields = klass.getDeclaredFields();
        logger.debug(marker, "Loading configuration");
        InputStream configInpuStream = Config.class.getClassLoader().getResourceAsStream(CONFIG_FILE_NAME);
        //InputStream credentialsInputStream = Config.class.getClassLoader().getResourceAsStream(CREDENTIALS_FILE_NAME);
        logger.debug(marker, "Configuration loaded");
        Properties configProperties = new Properties();
        Properties credentialsProperties = new Credentials();
        logger.debug(marker, "Parsing properties");
        try {
            configProperties.load(configInpuStream);
            //credentialsProperties.load(credentialsInputStream);
        } catch (IOException e) {
            logger.error(marker, "Unable to load configuration file", e);
            throw new IllegalStateException("Unable to load configuration file", e);
        }
        try {
            for (Field field : fields) {
                if (!field.isAnnotationPresent(Property.class)) continue;
                Property property = field.getAnnotation(Property.class);
                Class<?> fieldType = field.getType();
                Properties properties = property.source().equals(Source.CONFIG) ? configProperties : credentialsProperties;
                String propertyValue = properties.getProperty(property.name());
                logger.trace(marker, "Property source: \"{}\", property name: \"{}\",  parsed value: \"{}\"",
                        property.source(), property.name(), propertyValue);
                if (propertyValue == null) {
                    throw new ConfigurationInvalidException(String.format("Property value is null\nsource: \"%s\",name: \"%s\")", property.source(), property.name()));
                }
                if (fieldType.equals(String.class)) {
                    field.set(klass, propertyValue);
                } else if (fieldType.equals(URI.class)) {
                    URI uri = new URI(propertyValue);
                    field.set(klass, uri);
                } else if (fieldType.equals(URL.class)) {
                    URL uri = new URL(propertyValue);
                    field.set(klass, uri);
                } else if (fieldType.equals(int.class)) {
                    Integer value = Integer.valueOf(propertyValue);
                    field.set(klass, value);
                }
            }
            logger.debug(marker, "All annotated properties successfully parsed");
        } catch (IllegalAccessException e) {
            logger.error(marker, "", e);
            throw new IllegalStateException("Cannot get access to set field value", e);
        } catch (URISyntaxException | MalformedURLException e) {
            logger.error(marker, "Unable to parse uri from config file", e);
            throw new IllegalStateException("Unable to parse url from config file", e);
        }
    }

    private Config() {}


    public enum Source {
        CREDENTIALS,
        CONFIG
    }

    public static URL getBaseUrl() {
        return baseUrl;
    }

    public static URI getApiUrl() {
        return apiUrl;
    }

    public static String getUsername() {
        return username;
    }

    public static String getEmail() {
        return email;
    }

    public static String getPassword() {
        return password;
    }

    public static String getApiKey() {
        return apiKey;
    }

    public static String getApiToken() { return apiToken; }

    public static String getAppPakage() {
        return appPakage;
    }

    public static String getAppActivity() {
        return appActivity;
    }

    public static URL getSeleniumHubUrl() { return seleniumHubUrl; }

    public static int getGlobalTimeout() {
        return globalTimeout;
    }

}

package com.pflb.ttipoi.helpers.repositories;

import com.pflb.ttipoi.pages.AbstractPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.HashMap;
import java.util.Map;

public class PageStore {
    private static ThreadLocal<Map<String, Object>> repository = new InheritableThreadLocal<>();
    private static Logger logger = LoggerFactory.getLogger(PageStore.class);
    private static Marker marker = MarkerFactory.getMarker("REPOSITORY/PAGE");

    public static void setUp() {
        repository.set(new HashMap<>());
    }

    private static void putObject(Object page) {
        logger.trace(marker, "New page for store: \"{}\"", page);
        if (page == null) throw new IllegalStateException("Argument passed to PageStore#put cannot be null");
        repository.get().put(page.getClass().getSimpleName(), page);
    }

    public static <T extends AbstractPage> void put(T page) {
        putObject(page);
    }

    @SuppressWarnings("unchecked")
    public static <T> T get(Class<T> klass) {
        logger.trace(marker, "Returning page \"{}\" from store", klass);
        return (T)repository.get().get(klass.getSimpleName());
    }

    @SuppressWarnings("unchecked")
    public static <T> T getByName(String name) {
        logger.trace(marker, "Returning page \"{}\" from store", name);
        return (T)repository.get().get(name);
    }
}

package com.pflb.ttipoi.helpers.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.HashMap;
import java.util.Map;

public class Storage {
    private static Logger logger = LoggerFactory.getLogger(Storage.class);
    private static Marker marker = MarkerFactory.getMarker("REPOSITORY/OBJECT");
    private static ThreadLocal<Map<String, Object>> store = new ThreadLocal<>();

//    static { store.set(new HashMap<>()); }
    public static void setUp() {
        store.set(new HashMap<>());
    }

    public static void put(String key, Object value) {
        logger.trace(marker, "New item to store: key \"{}\", value: \'{}\"", key, value);
        if (value == null) throw new IllegalStateException("Put null value to storage is not allowed");
        store.get().put(key, value);
    }

    @SuppressWarnings("unchecked")
    public static <T> T get(String key) {
        Object value = store.get().get(key);
        if(!store.get().containsKey(key)) throw new IllegalStateException(String.format("Key \"%s\" not found in object repository", key));
        logger.trace(marker, "Getting  \"{}\" from storage, stored value is \"{}\"", key, value);
        return (T)value;
    }
}

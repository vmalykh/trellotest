Feature: android application
  Scenario: login to application
    When application is running
    Then on page "WelcomePage" click on button "Login"
    Then page "LoginPage" is appeared
    And on page "LoginPage" fill "Username" field from config parameter "Username"
    And on page "LoginPage" fill "Password" field from config parameter "Password"
    And on page "LoginPage" click on button "Submit"
    Then page "MainPage" is appeared
    And screen orientation changed to "portrait"

  Scenario: create board
    When page "MainPage" is appeared
    And on page "MainPage" click on button "+"
    Then page "CreateBoardPage" is appeared
    And on page "CreateBoardPage" fill "Board name" field with random text string with prefix "AT android board / " and save value to storage with key "Board name"
    And on page "CreateBoardPage" click on button "Ok"
    Then page "BoardPage" is appeared
    And on page "BoardPage" title equals "Storage => Board name"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared
    And page "MainPage" contains board with name from storage "Board name"

  Scenario: rename card list on board
    When page "MainPage" is appeared
    And on page "MainPage" select first board with name starts with "AT android board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" get list #1 and rename it to "FIRST"
    Then page "BoardPage" has list with name "FIRST"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: create list of cards on board
    When page "MainPage" is appeared
    And on page "MainPage" select first board with name starts with "AT android board"
    Then page "BoardPage" is appeared
    When on page "BoardPage" swipe to last right position
    And on page "BoardPage" click on button "Add list"
    And page "BoardPage" is updated
    And on page "BoardPage" fill "List name" field with random text string with prefix "AT list / " and save value to storage with key "List name"
    And on page "BoardPage" click on button "Edit confirm"
    Then on page "BoardPage" list with name "Storage => List name" is appeared
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: create new card in list
    When page "MainPage" is appeared
    And on page "MainPage" select first board with name starts with "AT android board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element stored as "List object" save title with key "List name"
    And on element List stored as "List object" click button "Add card"
    Then page "AddCardDialogPage" is appeared
    And on page "AddCardDialogPage" fill "Card name" field with random text string with prefix "AT card /" and save value to storage with key "Card name"
    And on page "AddCardDialogPage" click on button "Add card"
    Then page "BoardPage" is appeared
    And on element List with name "Storage => List name" appeared card with name from storage "Card name"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: add description to card
    When page "MainPage" is appeared
    And on page "MainPage" select first board with name starts with "AT android board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element stored as "List object" save title with key "List name"
    And on element List stored as "List object" find card with name starts with "AT card" and save to storage with key "Card object"
    And on element stored as "Card object" save title with key "Card name"
    And on element List stored as "List object" select card with name starts with "Storage => Card name"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" fill "Description" field with value "Test description"
    Then page "FullCardPage" is updated
    And on page "FullCardPage" click on button "Confirm"
    Then page "FullCardPage" is updated
    And on page "FullCardPage" click on button "Back"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element List stored as "List object" find card with name starts with "AT card" and save to storage with key "Card object"
    And check: element Card stored as "Card object" have "Description"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: add checklist to card
    When page "MainPage" is appeared
    And on page "MainPage" select first board with name starts with "AT android board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element stored as "List object" save title with key "List name"
    And on element List stored as "List object" find card with name starts with "AT card" and save to storage with key "Card object"
    And on element stored as "Card object" save title with key "Card name"
    And on element List stored as "List object" select card with name starts with "Storage => Card name"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" click on button "+"
    And page "FullCardPage" is updated
    And on page "FullCardPage" click on button "Add checklist"
#    And page "FullCardPage" is updated
    Then on page "FullCardPage" element checklist is appeared
    And on page "FullCardPage" get element "CheckList" and save to storage as "Checklist"
    And on page "FullCardPage" fill "New checklist item" field with value "CL item 1"
    And on page "FullCardPage" click on button "Confirm"
    And on page "FullCardPage" fill "New checklist item" field with value "CL item 2"
    And on page "FullCardPage" click on button "Confirm"
    And on page "FullCardPage" click on button "Confirm"
    Then checklist stored as "Checklist" has item "CL item 1"
    Then checklist stored as "Checklist" has item "CL item 2"
    And on page "FullCardPage" click on button "Back"
    Then page "BoardPage" is appeared
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: rename checklist
    When page "MainPage" is appeared
    And on page "MainPage" select first board with name starts with "AT android board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element stored as "List object" save title with key "List name"
    And on element List stored as "List object" find card with name starts with "AT card" and save to storage with key "Card object"
    And on element stored as "Card object" save title with key "Card name"
    And on element List stored as "List object" select card with name starts with "Storage => Card name"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" get element "CheckList" and save to storage as "Checklist"
    And on element stored as "Checklist" fill "Title" field with value "AT checklist"
#    Then page "FullCardPage" is updated
    And on page "FullCardPage" click on button "Confirm"
    And on element stored as "Checklist" title equals "AT checklist"
    And on page "FullCardPage" click on button "Back"
    Then page "BoardPage" is appeared
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: confirm one element of checklist
    When page "MainPage" is appeared
    And on page "MainPage" select first board with name starts with "AT android board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element stored as "List object" save title with key "List name"
    And on element List stored as "List object" find card with name starts with "AT card" and save to storage with key "Card object"
    And on element stored as "Card object" save title with key "Card name"
    And on element List stored as "List object" select card with name starts with "Storage => Card name"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" get element "CheckList" and save to storage as "Checklist"
    And on element "Checklist" stored as "Checklist" get item "CL item 1" and set state to "checked"
    And on page "FullCardPage" click on button "Back"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element stored as "List object" save title with key "List name"
    And on element List stored as "List object" find card with name equals "Storage => Card name" and save to storage with key "Card object"
    And check: element "Card" stored as "Card object" have icon "checklist" with label "1/2"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: move card to another list
    When page "MainPage" is appeared
    #create new list
    And on page "MainPage" select first board with name starts with "AT android board"
    Then page "BoardPage" is appeared
    When on page "BoardPage" swipe to last right position
    And on page "BoardPage" click on button "Add list"
    And page "BoardPage" is updated
    And on page "BoardPage" fill "List name" field with random text string with prefix "Drop target / " and save value to storage with key "Drop target list name"
    And on page "BoardPage" click on button "Edit confirm"
    Then on page "BoardPage" list with name "Storage => Drop target list name" is appeared
    #find card and target list
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "Source list object"
    And on element List stored as "Source list object" select card with name starts with "AT card"
    Then page "FullCardPage" is appeared
    And on element stored as "PageStore => FullCardPage" save title with key "Card title"
    And on page "FullCardPage" click on button "More"
    Then Action menu is appeared
    And on menu stored as "Menu" select entry "Перемещение карточки"
    Then page "MoveCardPage" is appeared
    And on page "MoveCardPage" click on button "List selector"
    Then Dropdown list is appeared
    And on menu stored as "Menu" select entry "Storage => Drop target list name"
    And on page "MoveCardPage" click on button "Move"
    Then page "BoardPage" is appeared
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" click on button "Back"
    Then page "BoardPage" is appeared
    Then on element List with name "Storage => Drop target list name" appeared card with name from storage "Card title"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: remove card from list
    When page "MainPage" is appeared
    And on page "MainPage" select first board with name starts with "AT android board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name equals "FIRST" and save to storage with key "List object"
    And on element List stored as "List object" click button "Add card"
    Then page "AddCardDialogPage" is appeared
    And on page "AddCardDialogPage" fill "Card name" field with random text string with prefix "AT card /" and save value to storage with key "Card name"
    And on page "AddCardDialogPage" click on button "Add card"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name equals "FIRST" and save to storage with key "List object"
    And on element List with name "FIRST" appeared card with name from storage "Card name"
    And on element List stored as "List object" select card with name equals "Storage => Card name"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" click on button "More"
    Then Action menu is appeared
    And on menu stored as "Menu" select entry "Удалить"
    Then page "OkCancelDialogPage" is appeared
    Then on page "OkCancelDialogPage" click on button "Ok"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name equals "FIRST" and save to storage with key "List object"
    And list stored as "List object" should not contains card with name "Storage => Card name"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: rename board
    When page "MainPage" is appeared
    And on page "MainPage" select first board with name starts with "AT android board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" click on button "Menu"
    Then Right side menu is appeared
    And on menu stored as "Menu" select entry "Настройки доски"
    Then page "BoardSettingsPage" is appeared
    And on page "BoardSettingsPage" click on button "Rename"
    Then page "EditDialogPage" is appeared
    And on page "EditDialogPage" fill "Name" field with random text string with prefix "AT android renamed board / " and save value to storage with key "Board name"
    And on page "EditDialogPage" click on button "Ok"
    Then page "BoardSettingsPage" is appeared
    Then on page "BoardSettingsPage" title equals "Storage => Board name"
    And on page "BoardPage" click on button "Back"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: logout
    When page "MainPage" is appeared
    And on page "MainPage" click on button "More"
    Then Action menu is appeared
    And on menu stored as "Menu" select entry "Настройки"
    Then page "MainSettingsPage" is appeared
    And on page "MainSettingsPage" swipe to last down position
    Then page "MainSettingsPage" is updated
    Then on page "MainSettingsPage" click on text "Выйти"
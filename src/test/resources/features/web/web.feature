Feature: web application
  Scenario: login to trello account
    Given navigate to main trello url
    Then page "LoginPage" is appeared
    When on page "LoginPage" fill "Username" field from config parameter "Username"
    And on page "LoginPage" fill "Password" field from config parameter "Password"
    And on page "LoginPage" click on button "Login"
    Then page "MainPage" is appeared

  Scenario: create new board
    Given page "MainPage" is appeared
    When on page "MainPage" click on button "Add board"
    Then page "CreateBoardDialog" is appeared
    And on page "CreateBoardDialog" fill "Board name" field with random text string with prefix "AT web board / " and save value to storage with key "Board name"
    And on page "CreateBoardDialog" click on button "Create board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" click on button "Close"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: create new list on board
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" click on button "Add a list"
    And on page "BoardPage" fill "List name" field with random text string with prefix "AT list / " and save value to storage with key "List name"
    And on page "BoardPage" click on button "Save"
    Then on page "BoardPage" list with name "Storage => List name" is appeared
    And on page "BoardPage" click on button "Close"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: create new card
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element List stored as "List object" click button "Add a card"
    And on element stored as "List object" fill "Card name" field with value "AT card"
    And on element List stored as "List object" click button "Add"
    Then on element stored as "List object" appeared card with name "AT card"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: add description to card
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element List stored as "List object" select card with name equals "AT card"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" click on button "Change description"
    And on page "FullCardPage" fill "Description" field with value "Some AT description"
    And on page "FullCardPage" click on button "Save description"
    And on page "FullCardPage" click on button "Close"
    And on element List stored as "List object" find card with name equals "AT card" and save to storage with key "Card object"
    Then check: element Card stored as "Card object" have "Description"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: add checklist to card
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element List stored as "List object" select card with name equals "AT card"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" click on button "Checklist"
    Then page "DialogPage" is appeared
    And on page "DialogPage" fill "Title" field with value "AT checklist"
    And on page "DialogPage" click on button "Confirm"
    Then on page "FullCardPage" element checklist is appeared
    And on page "FullCardPage" click on button "Close"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: add two elements to checklist
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element List stored as "List object" select card with name equals "AT card"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" get element "CheckList" and save to storage as "Checklist object"
    And on element stored as "Checklist object" click button "Add an item"
    And on element stored as "Checklist object" fill "Item name" field with value "Item to be checked"
    And on element stored as "Checklist object" click button "Add"
    And on element stored as "Checklist object" fill "Item name" field with value "Another item"
    And on element stored as "Checklist object" click button "Add"
    And on page "FullCardPage" click on button "Close"
    And on element List stored as "List object" find card with name equals "AT card" and save to storage with key "Card object"
    And check: element "Card" stored as "Card object" have icon "checklist" with label "0/2"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: confirm one checklist item
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "List object"
    And on element List stored as "List object" select card with name equals "AT card"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" get element "CheckList" and save to storage as "Checklist object"
    And on element "Checklist" stored as "Checklist object" get item "Item to be checked" and set state to "checked"
    And on page "FullCardPage" click on button "Close"
    And on element List stored as "List object" find card with name equals "AT card" and save to storage with key "Card object"
    And check: element "Card" stored as "Card object" have icon "checklist" with label "1/2"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: Move card to another list
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name starts with "AT list" and save to storage with key "Source list object"
    And on element List stored as "Source list object" find card with name equals "AT card" and save to storage with key "Card object"
    #create target list
    And on page "BoardPage" click on button "Add a list"
    And on page "BoardPage" fill "List name" field with value "Drop target"
    And on page "BoardPage" click on button "Save"
    And on page "BoardPage" find list with name equals "Drop target" and save to storage with key "Target list object"
    And move card stored as "Card object" to bottom of list stored as "Target list object"
    Then on element stored as "Target list object" appeared card with name "AT card"
    And on page "BoardPage" click on button "Close"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: rename card
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name equals "Drop target" and save to storage with key "List object"
    And on element List stored as "List object" select card with name equals "AT card"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" click on button "Rename"
    And on page "FullCardPage" fill "Card title" field with value "AT renamed card" and press Enter
    And on page "FullCardPage" click on button "Close"
    Then on element stored as "List object" appeared card with name "AT renamed card"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared


  Scenario: delete card
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name equals "Drop target" and save to storage with key "List object"
    And on element List stored as "List object" select card with name equals "AT renamed card"
    Then page "FullCardPage" is appeared
    And on page "FullCardPage" click on button "Archive"
    And on page "FullCardPage" click on button "Delete"
    Then page "DialogPage" is appeared
    And on page "DialogPage" click on button "Confirm"
    Then list stored as "List object" should not contains card with name "AT renamed card"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: rename list
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" find list with name equals "Drop target" and save to storage with key "List object"
    And on element List stored as "List object" click button "Rename"
    And press key delete
    And on element stored as "List object" fill "List name" field with value "List to delete" and press Enter
    Then on page "BoardPage" list with name "List to delete" is appeared
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: rename board
    Given page "MainPage" is appeared
    And on page "MainPage" click on button "Boards"
    Then page "BoardsListPage" is appeared
    When on page "BoardsListPage" select first board with name starts with "AT web board"
    Then page "BoardPage" is appeared
    And on page "BoardPage" click on button "Rename"
    And press key delete
    And on page "BoardPage" fill "Board title" field with value "AT web renamed board" and press Enter
    Then on page "BoardPage" title equals "AT web renamed board"
    And on page "BoardPage" click on button "Back"
    Then page "MainPage" is appeared

  Scenario: log out
    Given page "MainPage" is appeared
    And on page "MainPage" ivoke menu
    Then UserMenu is appeared
    And on menu stored as "Menu" select entry "Log Out"
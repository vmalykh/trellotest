package com.pflb.ttipoi;

import com.pflb.ttipoi.helpers.Config;
import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.repositories.PageStore;
import com.pflb.ttipoi.helpers.repositories.Storage;
import com.pflb.ttipoi.models.Board;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class AbstractTest {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeSuite
    public void clean() {
        Marker marker = MarkerFactory.getMarker("TEST_START_CLEAN");
        logger.debug(marker, "Cleaning old test data");
        Board.getAllBoards().stream().filter(b -> b.name.startsWith("AT ")).forEach(Board::remove);
    }

    @Parameters(value = "browserName")
    @BeforeGroups("GUI")
    public void setUp(String browserName) {
        Marker marker = MarkerFactory.getMarker("TEST_SETUP");
        logger.debug(marker, "Setup driver for name {}", browserName);
        PageStore.setUp();
        Storage.setUp();
        driver = Driver.forName(browserName);
        driver.manage().timeouts().implicitlyWait(Config.getGlobalTimeout(), TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, Config.getGlobalTimeout());
    }

    @AfterGroups("GUI")
    public void tearDown() {
        Marker marker = MarkerFactory.getMarker("TEST_SHUTDOWN");
        logger.debug(marker, "TEST_SHUTDOWN");
        driver.quit();
    }
}

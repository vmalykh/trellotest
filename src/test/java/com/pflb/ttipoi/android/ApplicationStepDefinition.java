package com.pflb.ttipoi.android;

import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.SearchPolicy;
import com.pflb.ttipoi.helpers.repositories.PageStore;
import com.pflb.ttipoi.helpers.repositories.Storage;
import com.pflb.ttipoi.interfaces.HasTitle;
import com.pflb.ttipoi.interfaces.Menu;
import com.pflb.ttipoi.interfaces.Scrollabe;
import com.pflb.ttipoi.pages.android.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.ScreenOrientation;
import org.testng.Assert;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.function.BiPredicate;

import static com.pflb.ttipoi.helpers.Utils.getName;

public class ApplicationStepDefinition {
    @Then("^page \"([^\"]*)\" is appeared")
    public void pageIsAppeared(String className) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<?> klass =  Class.forName("com.pflb.ttipoi.pages.android." + className);
        @SuppressWarnings("unchecked")
        Constructor<? extends AbstractAndroidPage> constructor = (Constructor<? extends AbstractAndroidPage>) klass.getDeclaredConstructor();
        PageStore.put(constructor.newInstance());
    }

    @Then("^page \"([^\"]*)\" is updated$")
    public void pageIsUpdated(String pageName) throws Throwable {
        pageIsAppeared(pageName);
    }

    @When("^application is running$")
    public void applicationIsRunning() {
        WelcomePage welcomePage = new WelcomePage();
        PageStore.put(welcomePage);
    }

    @When("^on page \"MainPage\" tap on board from storage \"([^\"]*)\"$")
    public void onPageTapOnBoardFromStorage(String stoargeKey) {
        MainPage page = PageStore.getByName("MainPage");
        String boardName = Storage.get(stoargeKey);
        page.selectBoard(boardName);
    }

    @And("^page \"MainPage\" contains board with name from storage \"([^\"]*)\"$")
    public void pageContainsBoardWithNameFromStorage(String storageKey) {
        MainPage page = PageStore.getByName("MainPage");
        String value = Storage.get(storageKey);
        boolean testResult =  page.getAllBoardNames().stream().anyMatch(name -> name.equals(value));
        Assert.assertTrue(testResult, String.format("BoardPage with name \"%s\" not found", value));
    }

    @Then("^on page \"BoardPage\" list with name \"([^\"]*)\" is appeared$")
    public void onPageListWithNameFromStorageValueIsAppeared(String listName) {
        BoardPage page = PageStore.getByName("BoardPage");
        listName = getName(listName);
        page.scroll(ScrollDirection.LEFT, -1);
        page = new BoardPage(); //update page
        ListPage list = page.getListByName(listName, SearchPolicy.EQUALS);
        Assert.assertNotNull(list, String.format("List with title \"%s\" not found ob board \"%s\"", list, page.getTitle()));
    }

    @When("^on page \"([^\"]*)\" swipe to last (left|right|up|down) position$")
    public void swipeToLastRightPosition(String pageName, String direction) {
        ScrollDirection scrollDirection = ScrollDirection.valueOf(direction.toUpperCase());
        Scrollabe page = PageStore.getByName(pageName);
        page.scroll(scrollDirection, -1);
    }

    @And("^on page \"BoardPage\" get list #(\\d+) and rename it to \"([^\"]*)\"$")
    public void getListAndStore(int listIndex, String newName) {
        BoardPage boardPage = PageStore.getByName("BoardPage");
        ListPage list = boardPage.getListByIndex(listIndex);
        list.click("List title");
        //update BoardPage and list
        boardPage = new BoardPage();
        list = boardPage.getListByIndex(listIndex);
        list.fill("List title", newName);
        boardPage.click("Edit confirm");
        //update BoardPage
        PageStore.put(new BoardPage());
    }

    @Then("^page \"BoardPage\" has list with name \"([^\"]*)\"$")
    public void pageHasListWithName(String listName) {
        BoardPage boardPage = PageStore.getByName("BoardPage");
        ListPage list = boardPage.getListByName(listName, SearchPolicy.EQUALS);
        Assert.assertNotNull(list, "List not found");
    }

    @And("^on element (Card|List) with name \"([^\"]*)\" appeared card with name from storage \"([^\"]*)\"$")
    public void chekForCardAppeared(String elementType, String listName, String cardNameStorageKey) {
        BoardPage page = PageStore.getByName("BoardPage");
        listName = getName(listName);
        String cardName = Storage.get(cardNameStorageKey);
        ListPage list = page.getListByName(listName, SearchPolicy.EQUALS);
        list.getCardByName(cardName, SearchPolicy.STARTS_WITH);
    }

    @And("^on element stored as \"([^\"]*)\" save title with key \"([^\"]*)\"$")
    public void saveElementTitle(String objectStorageKey, String titleStorageKey) {
        HasTitle element;
        if (objectStorageKey.contains("PageStore")) {
            int keyStartPosition = objectStorageKey.indexOf("=>") + 3;
            String key = objectStorageKey.substring(keyStartPosition);
            element = PageStore.getByName(key);
        }else {
            element = Storage.get(objectStorageKey);
        }
        Storage.put(titleStorageKey, element.getTitle());
    }

    @Then("^checklist stored as \"([^\"]*)\" has item \"([^\"]*)\"$")
    public void checklistStoredAsHasItem(String storageKey, String itemName) {
        CardCheckListPage checkList = Storage.get(storageKey);
        boolean itemExist = checkList.getItems().stream().anyMatch(item -> item.getTitle().equals(itemName));
        Assert.assertTrue(itemExist, String.format("Item with name \"%s\" does'nt exist in checklist \"%s\"", itemName, checkList.getTitle()));
    }

    @And("^on element stored as \"([^\"]*)\" title (starts with|equals|contains) \"([^\"]*)\"$")
    public void checkTitleConctainsText(String storageKey, String comparePolicyStr, String text) {
        HasTitle element = Storage.get(storageKey);
        BiPredicate<String, String> comparePolicy = SearchPolicy.valueOf(comparePolicyStr.replace(" ", "_").toUpperCase()).predicate();
        boolean result = comparePolicy.test(text, element.getTitle());
        Assert.assertTrue(result, String.format("Test title failed:\nCompare policy: %s\nexpected: %s\nfound: %s", comparePolicyStr, text, element.getTitle()));
    }

    @And("^screen orientation changed to \"(landscape|portrait)\"$")
    public void screenOrientationChengedTo(String screenOrientationStr) {
        ScreenOrientation screenOrientation = ScreenOrientation.valueOf(screenOrientationStr.toUpperCase());
        ((AndroidDriver)Driver.get()).rotate(screenOrientation);
    }

    @Then("^(Dropdown list|Action menu|Right side menu) is appeared$")
    public void initActionMenuPage(String menuType) {
        Menu menu;
        switch (menuType) {
            case "Action menu":
                menu = new ActionMenu();
                break;

            case "Dropdown list":
                menu = new DropDownListPage();
                break;

            case "Right side menu":
                menu = new RightSideMenu();
                break;

            default:
                throw new InvalidArgumentException("Unknown menu type: " + menuType);
        }
        Storage.put("Menu", menu);
    }

    @And("^navigate back$")
    public void navigateBack() {
        Driver.get().navigate().back();
    }

    @Then("^on page \"([^\"]*)\" click on text \"([^\"]*)\"$")
    public void onPageClickOnText(String pageName, String text) {
        Menu page = PageStore.getByName(pageName);
        page.selectMenuEntry(text);
    }
}

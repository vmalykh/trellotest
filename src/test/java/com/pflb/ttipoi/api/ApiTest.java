package com.pflb.ttipoi.api;

import com.pflb.ttipoi.AbstractTest;
import com.pflb.ttipoi.helpers.Utils;
import com.pflb.ttipoi.models.Board;
import com.pflb.ttipoi.models.Card;
import com.pflb.ttipoi.models.CheckList;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ApiTest extends AbstractTest {
    @Test
    public void createNewBoard() {
        //create new board
        String boardName = "AT rest board / " + Utils.uuid();
        new Board(boardName).save();

        //check for board exist
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.equals(boardName)).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), String.format("Board with name \"%s\" does'nt exist.", boardName));
    }

    @Test(dependsOnMethods = "createNewBoard")
    public void createNewListOnBoard() {
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.startsWith("AT rest board")).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), "Board with name starts with \"AT rest board\" does'nt exist");
        Board board = optionalBoard.get();

        //creating lew list
        String listName = "AT list / " + Utils.uuid();
        com.pflb.ttipoi.models.List list = new com.pflb.ttipoi.models.List(listName);
        board.createList(list);

        //check for list exists
        List<com.pflb.ttipoi.models.List> lists = board.getLists();
        boolean result = lists.stream().anyMatch(e -> e.name.equals(listName));
        Assert.assertTrue(result, String.format("can't find list with name \"%s\" on board \"%s\"", listName, board.name));
    }

    @Test(dependsOnMethods = "createNewListOnBoard")
    public void renameListOnBoard() {
        //finding board
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.startsWith("AT rest board")).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), "Board with name starts with \"AT rest board\" does'nt exist");
        Board board = optionalBoard.get();

        //requesting all lists
        List<com.pflb.ttipoi.models.List> lists = board.getLists();
        Optional<com.pflb.ttipoi.models.List> optionalList = lists.stream().filter(e -> e.name.startsWith("AT list")).findFirst();
        Assert.assertTrue(optionalList.isPresent(), String.format("List with name starts with \"AT list\" does'nt exist on board \"%s\"", board.name));
        com.pflb.ttipoi.models.List oldList = optionalList.get();

        String newListName = "AT list renamed / " + Utils.uuid();
        oldList.name = newListName;
        oldList.rename();

        //checking for list name changed
        lists = board.getLists();
        boolean result = lists.stream().anyMatch(e -> e.name.equals(newListName));
        Assert.assertTrue(result, String.format("can't find list with name \"%s\" on board \"%s\"", newListName, board.name));
    }

    @Test(dependsOnMethods = "createNewListOnBoard")
    public void createCardOnList() {
        //finding board
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.startsWith("AT rest board")).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), "Board with name starts with \"AT rest board\" does'nt exist");
        Board board = optionalBoard.get();

        //finding list
        List<com.pflb.ttipoi.models.List> lists = board.getLists();
        Optional<com.pflb.ttipoi.models.List> optionalList = lists.stream().filter(e -> e.name.startsWith("AT list")).findFirst();
        Assert.assertTrue(optionalList.isPresent(), String.format("List with name starts with \"AT list\" does'nt exist on board \"%s\"", board.name));
        com.pflb.ttipoi.models.List list = optionalList.get();

        //creating cards
        String commonCardName = "AT card / " + Utils.uuid();
        String movableCardName = "AT movable card / " + Utils.uuid();
        String cardToDelName = "AT del card / " + Utils.uuid();
        Card[] newCards = {new Card(commonCardName, null), new Card(movableCardName, null), new Card(cardToDelName, null)};
        Stream.of(newCards).forEach(list::createCard);
        List<String> namesList = new LinkedList<String>() {{add(commonCardName); add(movableCardName); add(cardToDelName);}};

        //checking card exist
        List<Card> cards = list.getCards();
//        boolean result = namesList.containsAll(resultCardNames);
        namesList.forEach(expectedName -> {
            boolean cardIsPresent = cards.stream().map(card -> card.name).collect(Collectors.toList()).contains(expectedName);
            Assert.assertTrue(cardIsPresent, String.format("Card with name \"%s\" not found on list \"%s\"", expectedName, list.name));
        });

    }

    @Test(dependsOnMethods = "createCardOnList")
    public void addDescriptionToCard() {
        //finding board
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.startsWith("AT rest board")).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), "Board with name starts with \"AT rest board\" does'nt exist");
        Board board = optionalBoard.get();

        //finding list and card
        com.pflb.ttipoi.models.List list = board.getListByNameStartsWith("AT list");
        Card card = list.getCardByNameStartsWith("AT card");

        //changing description
        String description = "Test description for card";
        card.desc = description;
        card.save();

        //checking for description changed
        Card changedCard = list.getCardByNameStartsWith(card.name);
        Assert.assertEquals(changedCard.desc, description, String.format("failed to change description on card \"%s\"", card.name));
    }

    @Test(dependsOnMethods = "createCardOnList")
    public void addChecklistToCard() {
        //finding board
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.startsWith("AT rest board")).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), "Board with name starts with \"AT rest board\" does'nt exist");
        Board board = optionalBoard.get();

        //finding list and card
        com.pflb.ttipoi.models.List list = board.getListByNameStartsWith("AT list");
        Card card = list.getCardByNameStartsWith("AT card");

        //addind new checklist to card
        CheckList checkList = card.addCheckList("AT checklist");
        checkList.addCheckItem("to be confirmed", null);
        checkList.addCheckItem("anoter item", null);

        //checking for presence
        List<CheckList.CheckItem> items = checkList.getCheckItems();
        Assert.assertEquals(items.get(0).name, "to be confirmed");
        Assert.assertEquals(items.get(1).name, "anoter item");
    }

    @Test(dependsOnMethods = "addChecklistToCard")
    public void confirmOneChecklistItem() {
        //finding board
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.startsWith("AT rest board")).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), "Board with name starts with \"AT rest board\" does'nt exist");
        Board board = optionalBoard.get();

        //finding list, card, checklist,checkitems
        com.pflb.ttipoi.models.List list = board.getListByNameStartsWith("AT list");
        Card card = list.getCardByNameStartsWith("AT card");
        Optional<CheckList> optionalCheckList = card.getChecklists()
                .stream().filter(cl -> cl.name.equals("AT checklist")).findFirst();
        Assert.assertTrue(optionalCheckList.isPresent(), "Cannot find checklist with name \"AT checklist\"");
        CheckList checkList = optionalCheckList.get();
        List<CheckList.CheckItem> checkItems = checkList.getCheckItems();
        Optional<CheckList.CheckItem> optionalCheckItem = checkItems.stream().filter(ci -> ci.name.equals("to be confirmed")).findFirst();
        Assert.assertTrue(optionalCheckItem.isPresent(), "Cannot find checkitem with name \"to be confirmed\"");
        CheckList.CheckItem checkItem = optionalCheckItem.get();
        checkItem.state = true;
        checkItem.save();

        //checking for checkitem state changed
        Optional<CheckList.CheckItem> optionalCheckItemForCheck = checkList.getCheckItems().stream().filter(ci -> ci.name.equals("to be confirmed")).findFirst();
        Assert.assertTrue(optionalCheckItemForCheck.isPresent(), "Cannot find checkitem with name \"to be confirmed\"");
        Assert.assertTrue(optionalCheckItemForCheck.get().state, "State of checklist item \"to be confirmed\" have not changed");
    }

    @Test(dependsOnMethods = "createNewListOnBoard")
    public void moveCardToAnotherList() {
        //finding board
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.startsWith("AT rest board")).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), "Board with name starts with \"AT rest board\" does'nt exist");
        Board board = optionalBoard.get();

        //finding source list and card
        com.pflb.ttipoi.models.List list = board.getListByNameStartsWith("AT list");
        Card card = list.getCardByNameStartsWith("AT movable card");

        //creating target list
        String targetListName = "AT target list / " + Utils.uuid();
        com.pflb.ttipoi.models.List targetList = new com.pflb.ttipoi.models.List(targetListName);
        board.createList(targetList);

        //moving card
        card.move(targetList);
        List<Card> cards = targetList.getCards();
        Optional<Card> optionalCard = cards.stream().filter(c -> c.name.equals(card.name)).findFirst();
        Assert.assertTrue(optionalCard.isPresent(), String.format("Cannot find card with name \"%s\" in target list \"%s\"", card.name, targetList.name));
    }

    @Test(dependsOnMethods = "createCardOnList")
    public void renameCard() {
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.startsWith("AT rest board")).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), "Board with name starts with \"AT rest board\" does'nt exist");
        Board board = optionalBoard.get();

        //finding source list and card
        com.pflb.ttipoi.models.List list = board.getListByNameStartsWith("AT list");
        Card card = list.getCardByNameStartsWith("AT card");

        //renaming
        card.name = "AT card renamed / " + Utils.uuid();
        card.save();
        Card renamedCard = list.getCardByNameStartsWith("AT card renamed ");
        Assert.assertNotNull(renamedCard, String.format("Card rename failed on list \"%s\"", list.name));
    }

    @Test(dependsOnMethods = "createCardOnList")
    public void deleteCard() {
        //finding board
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.startsWith("AT rest board")).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), "Board with name starts with \"AT rest board\" does'nt exist");
        Board board = optionalBoard.get();

        //finding source list and card
        com.pflb.ttipoi.models.List list = board.getListByNameStartsWith("AT list");
        Card card = list.getCardByNameStartsWith("AT del card");

        //deleting card
        card.delete();
        List<Card> cards = list.getCards();
        boolean result = cards.stream().noneMatch(c -> c.name.equals(card.name));
        Assert.assertTrue(result, String.format("Card still present on list \"%s\"", list.name));
    }

    @Test(dependsOnMethods = "createNewBoard")
    public void renameBoard() {
        //finding board
        List<Board> boardsList = Board.getAllBoards();
        Optional<Board> optionalBoard = boardsList.stream().filter(e -> e.name.startsWith("AT rest board")).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), "Board with name starts with \"AT rest board\" does'nt exist");
        Board board = optionalBoard.get();

        //renaming board
        String newBoardName = "AT rest board renamed / " + Utils.uuid();
        board.name = newBoardName;
        board.save();

        //checking
        List<Board> boards = Board.getAllBoards();
        boolean result = boards.stream().anyMatch(b -> b.name.equals(newBoardName));
        Assert.assertTrue(result, String.format("Renamed board with name \"%s\" not found in list of all boards", newBoardName));
    }
}

package com.pflb.ttipoi.common;

import com.pflb.ttipoi.exceptions.InvalidOperationException;
import com.pflb.ttipoi.helpers.Config;
import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.SearchPolicy;
import com.pflb.ttipoi.helpers.Utils;
import com.pflb.ttipoi.helpers.repositories.PageStore;
import com.pflb.ttipoi.helpers.repositories.Storage;
import com.pflb.ttipoi.interfaces.*;
import cucumber.api.java.AfterStep;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static com.pflb.ttipoi.helpers.Utils.getName;

public class CommonStepDefinition {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private Marker marker = MarkerFactory.getMarker("STEPS/COMMON");

    @Then("^on page \"([^\"]*)\" fill \"([^\"]*)\" field with value \"([^\"]*)\"( and press Enter)?$")
    public void fillFieldWithValue(String pageName, String fieldName, String value, String pressEnter) {
        Fillable page = PageStore.getByName(pageName);
        if (pressEnter != null) value += "\n";
        page.fill(fieldName, value);
    }

    @Then("^on page \"([^\"]*)\" fill \"([^\"]*)\" field from config parameter \"([^\"]*)\"$")
    public void userFillFieldFromConfig(String pageName, String fieldName, String configParameterName) {
        String value;
        switch (configParameterName) {
            case "Username":
                value = Config.getUsername();
                break;

            case "Password":
                value = Config.getPassword();
                break;

            default:
                throw new InvalidOperationException(String.format("Parameter \"%s\" not found or not allowed here", configParameterName));
        }
        fillFieldWithValue(pageName, fieldName, value, null);
    }

    @Then("^on page \"([^\"]*)\" click on button \"([^\"]*)\"$")
    public void clickOnButton(String pageName, String btnName) {
        Clickable page = PageStore.getByName(pageName);
        page.click(btnName);
    }

    @And("^on page \"([^\"]*)\" fill \"([^\"]*)\" field with random text string with prefix \"([^\"]*)\" and save value to storage with key \"([^\"]*)\"$")
    public void fillByRandomString(String pageName, String fieldName, String prefix, String storageKey) {
        String value = prefix + Utils.uuid();
        Storage.put(storageKey, value);
        fillFieldWithValue(pageName, fieldName, value, null);
    }

    @And("^on page \"([^\"]*)\" select first board with name starts with \"([^\"]*)\"$")
    public void onPageSelectBoardWithNameStartsWith(String pageName, String prefix) {
        HasBoards page = PageStore.getByName(pageName);
        List<String> boards = page.getAllBoardNames();
        Optional<String> optionalBoard = boards.stream().filter(name -> name.startsWith(prefix)).findFirst();
        Assert.assertTrue(optionalBoard.isPresent(), String.format("Board with name \"%s\" not found on main page", prefix));
        page.selectBoard(optionalBoard.get());
    }

    @And("^on page \"BoardPage\" find list with name (starts with|equals) \"([^\"]*)\" and save to storage with key \"([^\"]*)\"$")
    public void getList(String searchPolicyStr, String listName, String storageKey) {
        listName = getName(listName);
        HasLists boardPage = PageStore.getByName("BoardPage");
        HasCards list = boardPage.getListByName(listName, SearchPolicy.valueOf(searchPolicyStr.replace(" ", "_").toUpperCase()));
        Storage.put(storageKey, list);
    }

    @And("^on element stored as \"([^\"]*)\" fill \"([^\"]*)\" field with value \"([^\"]*)\"( and press Enter)?$")
    public void checklistFillField(String storageKey, String fieldName, String value, String pressEnter) {
        Fillable element = Storage.get(storageKey);
        if (pressEnter != null) value += "\n";
        element.fill(fieldName, value);
    }

    @And("^on element (Card|List)? ?stored as \"([^\"]*)\" click button \"([^\"]*)\"$")
    public void onElementListStoredWithClickButton(String elementType, String storageKey, String btnName) {
        Clickable element = Storage.get(storageKey);
        element.click(btnName);
    }

    @And("^on element List stored as \"([^\"]*)\" find card with name (starts with|equals|contains) \"([^\"]*)\" and save to storage with key \"([^\"]*)\"$")
    public void FindCardAndSave(String listStorageKey, String searchPolicyStr, String cardTitle, String cardStorageKey) {
        SearchPolicy policy = SearchPolicy.valueOf(searchPolicyStr.replace(" ", "_").toUpperCase());
        HasCards list = Storage.get(listStorageKey);
        cardTitle = getName(cardTitle);
        SmallCard card = list.getCardByName(cardTitle, policy);
        Storage.put(cardStorageKey, card);
    }

    @And("^on element List stored as \"([^\"]*)\" select card with name (starts with|equals|contains) \"([^\"]*)\"$")
    public void selectCard(String listStorageKey, String searchPolicyStr, String cardTitle) {
        String name = getName(cardTitle);
        SearchPolicy policy = SearchPolicy.valueOf(searchPolicyStr.replace(" ", "_").toUpperCase());
        HasCards list = Storage.get(listStorageKey);
        SmallCard card = list.getCardByName(name, policy);
        ((Selectable)card).select();
    }

    @And("^check: element Card stored as \"([^\"]*)\" have \"(Description|Checklist)\"$")
    public void checkIconOnCard(String storageKey, String componentType) {
        SmallCard card = Storage.get(storageKey);
        Predicate<SmallCard> p = componentType.equals("Description") ? SmallCard::hasDescriptionIcon : SmallCard::hasChecklistIcon;
        Assert.assertTrue(p.test(card), String.format("%s can't be found on card %s", componentType, card.getTitle()));
    }

    @Then("^on page \"FullCardPage\" element checklist is appeared$")
    public void checklistIsAppeared() {
        HasCheckList page = PageStore.getByName("FullCardPage");
        Assert.assertTrue(page.hasCheckList(), "CheckList does'nt appeared!");
    }

    @And("^on page \"FullCardPage\" get element \"CheckList\" and save to storage as \"([^\"]*)\"$")
    public void getChecklistAndSave(String storageKey) {
        HasCheckList page = PageStore.getByName("FullCardPage");
        CheckList checkList = page.getCheckList();
        Storage.put(storageKey, checkList);
    }

    @And("^check: element \"Card\" stored as \"([^\"]*)\" have icon \"(checklist|description)\"( with label \"(\\d+/\\d+)\")?$")
    public void checkElementStoredAsHaveIconWithLabel(String storageKey, String icon, String expectedLabel){
        SmallCard card = Storage.get(storageKey);
        Predicate<SmallCard> predicate = icon.equals("checklist") ? SmallCard::hasChecklistIcon : SmallCard::hasDescriptionIcon;
        Assert.assertTrue(predicate.test(card), String.format("Element card with name \"%s\" does'nt have icon \"%s\"", card.getTitle(), icon));
        if (expectedLabel == null) return;
        if (icon.equals("description")) throw new IllegalStateException("Description icon cannot have label");
        WebDriverWait wait = new WebDriverWait(Driver.get(), 5);
        AtomicReference<String> errorMessage = new AtomicReference<>();
        try {
            wait.until(d -> {
                int[] checkedCount = card.getCheckedCount();
                int[] expected = new int[2];
                String[] splitted = expectedLabel.split("/");
                IntStream.range(0, splitted.length).forEach(i -> expected[i] = Integer.valueOf(splitted[i]));
                errorMessage.set(String.format("Arrays in not equals.\nExpected: %s\nBut actual: %s\n", Arrays.toString(expected), Arrays.toString(checkedCount)));
                logger.trace(marker, errorMessage.get());
                return Arrays.equals(checkedCount, expected);
            });
        } catch (WebDriverException e) {
            Assert.fail(errorMessage.get());
        }

//        Assert.assertTrue(Arrays.equals(checkedCount, expected), String.format("Arrays in not equals.\nExpected: %s\nBut actual: %s\n",
//                Arrays.toString(expected), Arrays.toString(checkedCount)));
    }

    @And("^on element \"Checklist\" stored as \"([^\"]*)\" get item \"([^\"]*)\" and set state to \"(checked|unchecked)\"$")
    public void getChecklistItem(String clStorageKey, String itemName, String state) {
        CheckList checkList = Storage.get(clStorageKey);
        checkList.setItemState(itemName, state.equals("checked"));
//        Optional<CheckList.Item> clItem = checkList.getItems().stream().filter(item -> item.getTitle().equals(itemName)).findFirst();
//        Assert.assertTrue(clItem.isPresent(), String.format("Item with name \"%s\" not found in checklist \"%s\"", itemName, checkList.getTitle()));
//        CheckList.Item item = clItem.get();
//        item.setChecked(state.equals("checked"));
    }

    @And("^list stored as \"([^\"]*)\" should not contains card with name \"([^\"]*)\"$")
    public void listStoredAsHaveNotCardWithName(String listStorageKey, String cardName) {
        HasCards list = Storage.get(listStorageKey);
        List<SmallCard> cards = list.getCards();
        boolean result = cards.stream().noneMatch(c -> c.getTitle().equals(cardName));
        Assert.assertTrue(result, "Card still present in list");
    }

    @And("^on page \"([^\"]*)\" title (equals|contains) \"([^\"]*)\"$")
    public void onPageTitleEqualsStorageValue(String pageName, String comparePolicy, String name) {
        name = getName(name);
        HasTitle page = PageStore.getByName(pageName);

        String currentTitle = page.getTitle();

        String errorMessage = String.format("Current title \"%s\" not %s  \"%s\"", currentTitle, comparePolicy, name);
        if (name.equals("equals")) Assert.assertEquals(name, currentTitle, errorMessage);
        else Assert.assertTrue(currentTitle.contains(name), errorMessage);
    }

    @And("^on (element stored as|page) \"([^\"]*)\" ivoke menu$")
    public void onElementStoredAsIvokeMenu(String type, String storageKey) {
        HasMenu element;
        if (type.equals("page")) element = PageStore.getByName(storageKey);
        else element = PageStore.getByName(storageKey);
        element.invokeMenu();
    }

    @And("^on menu stored as \"([^\"]*)\" select entry \"([^\"]*)\"$")
    public void onPageSelectItem(String storageKey, String menuEntryText) {
        menuEntryText = getName(menuEntryText);
        Menu menu = Storage.get(storageKey);
        menu.selectMenuEntry(menuEntryText);
    }

    @AfterStep
    @Attachment
    public byte[] afterStepHook() {
        return ((TakesScreenshot)Driver.get()).getScreenshotAs(OutputType.BYTES);
    }
}
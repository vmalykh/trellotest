package com.pflb.ttipoi.web;

import com.pflb.ttipoi.AbstractTest;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.PickleEventWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@CucumberOptions(
        features = "src/test/resources/features/web",
        glue = {"com.pflb.ttipoi.web", "com.pflb.ttipoi.common"},
        strict = true
)
public class WebCucumberRunner extends AbstractTest {
    private TestNGCucumberRunner runner;

    @BeforeClass(alwaysRun = true)
    public void setUpClass() {
        runner = new TestNGCucumberRunner(this.getClass());
    }

    @Test(dataProvider = "features", groups = "GUI")
    public void feature(PickleEventWrapper pickleEvent, CucumberFeatureWrapper cucumberFeature) throws Throwable {
        runner.runScenario(pickleEvent.getPickleEvent());
    }

    @DataProvider
    public Object[][] features() {
        return runner.provideScenarios();
    }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() {
        runner.finish();
    }
}

package com.pflb.ttipoi.web;

import com.pflb.ttipoi.helpers.Config;
import com.pflb.ttipoi.helpers.Driver;
import com.pflb.ttipoi.helpers.SearchPolicy;
import com.pflb.ttipoi.helpers.repositories.PageStore;
import com.pflb.ttipoi.helpers.repositories.Storage;
import com.pflb.ttipoi.interfaces.HasCards;
import com.pflb.ttipoi.interfaces.SmallCard;
import com.pflb.ttipoi.pages.AbstractPage;
import com.pflb.ttipoi.pages.web.BoardPage;
import com.pflb.ttipoi.pages.web.ListPage;
import com.pflb.ttipoi.pages.web.SmallCardPage;
import com.pflb.ttipoi.pages.web.UserMenu;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

import static com.pflb.ttipoi.helpers.Utils.getName;

public class WebStepDefinition {
    @Then("^page \"([^\"]*)\" is appeared")
    public void pageIsAppeared(String className) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<?> klass =  Class.forName("com.pflb.ttipoi.pages.web." + className);
        Constructor<? extends AbstractPage> constructor = (Constructor<? extends AbstractPage>) klass.getDeclaredConstructor();
        PageStore.put(constructor.newInstance());
    }

    @Then("^page \"([^\"]*)\" is updated$")
    public void pageIsUpdated(String pageName) throws Throwable {
        pageIsAppeared(pageName);
    }

    @Given("^navigate to main trello url$")
    public void openMainPage() {
        Driver.get().navigate().to(Config.getBaseUrl());
    }

    @Then("^on page \"BoardPage\" list with name \"([^\"]*)\" is appeared$")
    public void onPageListWithNameIsAppeared(String listName) {
        listName = getName(listName);
        BoardPage page = new BoardPage(); //update page
        HasCards list = page.getListByName(listName, SearchPolicy.EQUALS);
        Assert.assertNotNull(list, String.format("List with title \"%s\" not found ob board \"%s\"", listName, page.getTitle()));
    }

    @Then("^on element stored as \"([^\"]*)\" appeared card with name \"([^\"]*)\"$")
    public void cardIsAppeared(String storageKey, String cardName) {
        String finalCardName = getName(cardName);
        HasCards list = Storage.get(storageKey);
        List<SmallCard> cards = list.getCards();
        Optional<SmallCard> optionalCard = cards.stream().filter(card -> card.getTitle().equals(finalCardName)).findFirst();
        Assert.assertTrue(optionalCard.isPresent(), String.format("Card with name \"%s\" not found in list \"%s\"", finalCardName, list.getTitle()));
    }

    @And("^move card stored as \"([^\"]*)\" to (top|bottom) of list stored as \"([^\"]*)\"$")
    public void moveCardStoredAsToListStoredAs(String cardStorageKey, String disposition, String targetStorageKey) {
        SmallCardPage card = Storage.get(cardStorageKey);
        ListPage targetList = Storage.get(targetStorageKey);
        ListPage.DropLocation location = ListPage.DropLocation.valueOf(disposition.toUpperCase());
        card.moveTo(targetList, location);
    }

    @Then("UserMenu is appeared")
    public void initActionMenuPage() {
        Storage.put("Menu", new UserMenu());
    }

    @And("^press key (enter|delete)$")
    public void sendEnter(String keyName) {
        Keys key = keyName.equals("enter") ? Keys.RETURN : Keys.DELETE;
        new Actions(Driver.get()).sendKeys(key).build().perform();
    }
}
